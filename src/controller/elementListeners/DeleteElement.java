package controller.elementListeners;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Iterator;

import javax.swing.Action;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import controller.AbstractActions;
import core.MainFrame;
import model.elements.InputOutputElement;
import model.elements.SlotElement;
import model.elements.deviceElements.SlotDevice;
import model.elements.linkElements.LinkElement;
import tree.nodes.ElementNode;
import view.Diagram;
import view.Page;
import view.Slot;

public class DeleteElement extends AbstractActions {

	private static final long serialVersionUID = 1L;

	public DeleteElement() {
		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_D, ActionEvent.ALT_MASK));
		putValue(Action.SMALL_ICON, loadIcon("/icons/menuIcons/DeleteIcon.png"));
		putValue(Action.NAME, "Delete Element");
		putValue(Action.SHORT_DESCRIPTION, "Delete Element");
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Diagram diagram = MainFrame.getInstance().getSelectedDiagram();
		Page page = diagram.getSelectedPage();
		Slot slot = page.getSelectedSlot();

		if (!slot.getSlotNode().getSelectionModel().getSelectionList().isEmpty()) {
			Iterator<SlotElement> it = slot.getSlotNode().getSelectionModel().getSelectionList().iterator();

			while (it.hasNext()) {
				SlotElement element = it.next();
				slot.getSlotNode().getSlotModel().removeElement(element);

				// Prolazimo kroz link nodove, ako ima isto ime sa elementom brisemo ga iz
				// drveta
				for (int i = 0; i < slot.getSlotNode().getFolders().get(1).getElements().size(); i++) {
					ElementNode elementNode = slot.getSlotNode().getFolders().get(1).getElements().get(i);
					if (elementNode.getName().equals(element.getName())) {
						slot.getSlotNode().getFolders().get(1).getElements().remove(i);
					}
				}

				// Ako je selektovani device
				if (element instanceof SlotDevice) {
					SlotDevice device = (SlotDevice) element;

					// Proveravamo da li output sadrzi vezu
					for (LinkElement link : device.getFirstOutput().getLinks()) {
						slot.getSlotNode().getSlotModel().removeElement(link);

						for (int i = 0; i < slot.getSlotNode().getFolders().get(1).getElements().size(); i++) {
							ElementNode elementNode = slot.getSlotNode().getFolders().get(1).getElements().get(i);
							if (elementNode.getName().equals(link.getName())) {
								slot.getSlotNode().getFolders().get(1).getElements().remove(i);
							}
						}
					}

					// Proveravamo da li input sadrzi vezu
					for (InputOutputElement input : device.getInputs()) {
						for (LinkElement link : input.getLinks()) {
							slot.getSlotNode().getSlotModel().removeElement(link);

							for (int i = 0; i < slot.getSlotNode().getFolders().get(1).getElements().size(); i++) {
								ElementNode elementNode = slot.getSlotNode().getFolders().get(1).getElements().get(i);
								if (elementNode.getName().equals(link.getName())) {
									slot.getSlotNode().getFolders().get(1).getElements().remove(i);
								}
							}
						}

					}

				}

				SwingUtilities.updateComponentTreeUI(MainFrame.getInstance().getTree());
			}

			slot.getSlotNode().getSelectionModel().removeAllFromSelectionList();
			slot.repaint();
		}

	}

}
