package controller.elementListeners;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.KeyStroke;

import controller.AbstractActions;
import core.MainFrame;
import view.Diagram;
import view.Page;
import view.Slot;
import view.gui.Dialog;

public class SelectAllAction extends AbstractActions {

	private static final long serialVersionUID = 1L;

	public SelectAllAction() {
		
		putValue(Action.NAME, "Select All");
		putValue(Action.SHORT_DESCRIPTION, "Select All");
		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK));
		
	}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			Diagram diagram = MainFrame.getInstance().getSelectedDiagram();
			if (diagram == null) {
				Dialog.message("Please open a diagram first!");
				return;
			}
	
			Page page = diagram.getSelectedPage();
			if (page == null) {
				Dialog.message("Please open a page first!");
				return;
			}
	
			Slot slot = page.getSelectedSlot();
			if (slot == null) {
				Dialog.message("Please open a slot first");
				return;
			}
	
			slot.getSlotNode().getSelectionModel().addToSelectionList(slot.getSlotNode().getSlotModel().getElements());
	}

}
