package controller.elementListeners;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.KeyStroke;

import controller.AbstractActions;
import core.MainFrame;
import model.SlotElementsSelection;
import view.Diagram;
import view.Page;
import view.Slot;
import view.gui.Dialog;

public class CopyAction extends AbstractActions {

	private static final long serialVersionUID = 1L;

	public CopyAction() {
		putValue(Action.NAME, "Copy");
		putValue(Action.SHORT_DESCRIPTION, "Copy");
		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK));
		putValue(Action.SMALL_ICON, loadIcon("/icons/menuIcons/CopyIcon.png"));
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Diagram diagram = MainFrame.getInstance().getSelectedDiagram();
		if (diagram == null) {
			Dialog.message("Please open a diagram first!");
			return;
		}

		Page page = diagram.getSelectedPage();
		if (page == null) {
			Dialog.message("Please open a page first!");
			return;
		}

		Slot slot = page.getSelectedSlot();
		if (slot == null) {
			Dialog.message("Please open a slot first");
			return;
		}

		if (slot.getSlotNode().getSelectionModel().getSelectionListSize() != 0) {
			
			SlotElementsSelection contents = new SlotElementsSelection(
					slot.getSlotNode().getSelectionModel().getSelected());
			
			
			MainFrame.getInstance().getClipboard().setContents(contents, MainFrame.getInstance());
		}

	}

}
