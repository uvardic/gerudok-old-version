package controller.desktopListeners;

import java.awt.event.ActionEvent;

import javax.swing.Action;
import javax.swing.JInternalFrame;

import controller.AbstractActions;
import core.MainFrame;

public class TileV extends AbstractActions {

	private static final long serialVersionUID = 1L;

	// TileVAction je naziv klase a pun naziv je "Tile Vertical Action".
	// TileVAction nasleÄ‘uje "AbstractActions".

	// U konstruktoru definiÅ¡emo veliÄ�inu ikonice, kao i putanju do ikonice koja
	// se
	// nalazi u package-u icons. TakoÄ‘e definiÅ¡emo naziv i opis dijagrama u ovoj
	// akciji.

	public TileV() {
		putValue(Action.SMALL_ICON, loadIcon("/icons/menuIcons/TileV.png"));
		putValue(Action.NAME, "Tile Frames Vertically");
		putValue(Action.SHORT_DESCRIPTION, "Tile Frames Vertically");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (MainFrame.getInstance().getDiagramCount() == 0) {
			return;
		}

		JInternalFrame[] frames = new JInternalFrame[MainFrame.getInstance().getDiagramCount()];

		for (int i = 0; i < frames.length; i++) {
			
			frames[i] = MainFrame.getInstance().getDesktopPane().getAllFrames()[i];
			
		}

		int k = 0;

		int rows = (int) Math.ceil(frames.length / Math.floor(Math.sqrt(frames.length)));
		
		int columns = (int) Math.sqrt(frames.length);

		int x = (int) MainFrame.getInstance().getDesktopPane().getSize().getWidth() / rows;
		
		int y = (int) MainFrame.getInstance().getDesktopPane().getSize().getHeight() / columns;

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				if (k < MainFrame.getInstance().getDiagramCount()) {
					frames[k].setLocation(x * i, y * j);
					frames[k].setSize(x, y);
					k++;
				}
				
			}
			
		}
		

	}

}
