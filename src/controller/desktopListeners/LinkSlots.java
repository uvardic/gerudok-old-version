package controller.desktopListeners;

import java.awt.event.ActionEvent;

import javax.swing.Action;

import controller.AbstractActions;
import core.MainFrame;
import tree.nodes.SlotNode;
import tree.nodes.WorkspaceNode;
import view.Diagram;
import view.Page;
import view.Slot;

public class LinkSlots extends AbstractActions {

	private static final long serialVersionUID = 1L;

	public LinkSlots() {
		putValue(Action.NAME, "Link slots");
		putValue(Action.SHORT_DESCRIPTION, "Link current slot with last");
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (((WorkspaceNode) MainFrame.getInstance().getTreeModel().getRoot()).getProjects().size() < 2) {
			return;
		}
		
		Object o = MainFrame.getInstance().getTree().getLastSelectedPathComponent();
		
		if (o instanceof SlotNode) {
			SlotNode slotNode = (SlotNode) o;
			
			Diagram lastDiagram = MainFrame.getInstance().getLastDiagram();
			Page lastPage = lastDiagram.getSelectedPage();
			Slot lastSlot = lastPage.getSelectedSlot();
			
			slotNode.setSlotModel(lastSlot.getSlotNode().getSlotModel());
		}
	}

}
