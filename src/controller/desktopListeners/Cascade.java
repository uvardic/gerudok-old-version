package controller.desktopListeners;

import java.awt.event.ActionEvent;
import java.beans.PropertyVetoException;

import javax.swing.Action;
import javax.swing.JInternalFrame;

import controller.AbstractActions;
import core.MainFrame;
import utilities.Constants;

public class Cascade extends AbstractActions {

	private static final long serialVersionUID = 1L;
	// CascadeAction nasleÄ‘uje "AbstractActions".

	// U konstruktoru definiÅ¡emo veliÄ�inu ikonice, kao i putanju do ikonice koja
	// se
	// nalazi u package-u icons. TakoÄ‘e definiÅ¡emo naziv i opis dijagrama u ovoj
	// akciji.

	public Cascade() {
		putValue(Action.SMALL_ICON, loadIcon("/icons/menuIcons/Cascade.png"));
		putValue(Action.NAME, "Cascade Frames");
		putValue(Action.SHORT_DESCRIPTION, "Cascade Frames");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (MainFrame.getInstance().getDiagramCount() == 0) {
			return;
		}

		JInternalFrame[] frames = new JInternalFrame[MainFrame.getInstance().getDiagramCount()];

		for (int i = 0; i < frames.length; i++) {
			
			frames[i] = MainFrame.getInstance().getDesktopPane().getAllFrames()[i];
		}

		for (int i = frames.length - 1; i >= 0; i--) {
			frames[i].setLocation((frames.length - i) * Constants.xOFFSET, (frames.length - i) * Constants.yOFFSET);
			frames[i].setSize(Constants.WIDTH / 2, Constants.HEIGHT / 2);

			try {
				
				frames[i].setSelected(true);
				
			}
			catch (PropertyVetoException ex) {
				
				ex.printStackTrace();
				
			}
			
		}
		
		
	}
	

	
}
