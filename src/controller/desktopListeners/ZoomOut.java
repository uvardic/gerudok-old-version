package controller.desktopListeners;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.KeyStroke;

import controller.AbstractActions;
import core.MainFrame;
import view.Diagram;
import view.Page;
import view.Slot;
import view.gui.Dialog;

public class ZoomOut extends AbstractActions {

	private static final long serialVersionUID = 1L;

	public ZoomOut() {
		
		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_MINUS, ActionEvent.CTRL_MASK));
		putValue(Action.SMALL_ICON, loadIcon("/icons/menuIcons/ZoomOutIcon.png"));
		putValue(Action.NAME, "Zoom out");
		putValue(Action.SHORT_DESCRIPTION, "Zoom out");
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Diagram diagram = MainFrame.getInstance().getSelectedDiagram();
		if (diagram == null) {
			
			Dialog.message("Please open a diagram first!");
			return;
			
		}

		Page page = diagram.getSelectedPage();
		if (page == null) {
			
			Dialog.message("Please open a page first!");
			return;
			
		}

		Slot slot = page.getSelectedSlot();
		if (slot == null) {
			
			Dialog.message("Please open a slot first");
			return;
			
		}

		slot.centerZoom(false);
		
	}

}
