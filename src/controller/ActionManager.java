package controller;

import controller.desktopListeners.Cascade;
import controller.desktopListeners.LinkSlots;
import controller.desktopListeners.TileH;
import controller.desktopListeners.TileV;
import controller.desktopListeners.ZoomIn;
import controller.desktopListeners.ZoomOut;
import controller.desktopListeners.ZoomToFit;
import controller.elementListeners.CopyAction;
import controller.elementListeners.CutAction;
import controller.elementListeners.DeleteElement;
import controller.elementListeners.PasteAction;
import controller.elementListeners.RedoAction;
import controller.elementListeners.SelectAllAction;
import controller.elementListeners.UndoAction;
import controller.paletteListener.CircleAction;
import controller.paletteListener.LinkAction;
import controller.paletteListener.RectangleAction;
import controller.paletteListener.SelectAction;
import controller.paletteListener.TextAction;
import controller.paletteListener.TriangleAction;
import controller.serializationListeners.OpenProjectAction;
import controller.serializationListeners.SaveProjectAction;
import controller.treeListeners.DefaultSlotNode;
import controller.treeListeners.DeleteTreeNode;
import controller.treeListeners.NewTreeNode;

public class ActionManager {

	// ActionManager omogućava pregled svih akcija koje se izvršavaju
	// na zadatom projektu. Ova klasa takođe služi da pomoću
	// nje imamo pristup svim akcijama.

	private NewTreeNode newTreeNode = new NewTreeNode();
	private DefaultSlotNode defaultSlotNode = new DefaultSlotNode();
	private DeleteTreeNode deleteTreeNode = new DeleteTreeNode();

	private Cascade cascade = new Cascade();
	private TileH tileH = new TileH();
	private TileV tileV = new TileV();
	private ZoomIn zoomIn = new ZoomIn();
	private ZoomOut zoomOut = new ZoomOut();
	private ZoomToFit zoomToFit = new ZoomToFit();
	private LinkSlots linkSlots = new LinkSlots();

	private CircleAction circleAction = new CircleAction();
	private RectangleAction rectangleAction = new RectangleAction();
	private TriangleAction triangleAction = new TriangleAction();
	private SelectAction selectAction = new SelectAction();
	private LinkAction linkAction = new LinkAction();
	private TextAction textAction = new TextAction();

	private DeleteElement deleteElement = new DeleteElement();
	private RedoAction redoAction = new RedoAction();
	private UndoAction undoAction = new UndoAction();
	private CopyAction copyAction = new CopyAction();
	private CutAction cutAction = new CutAction();
	private PasteAction pasteAction = new PasteAction();
	private SelectAllAction selectAllAction = new SelectAllAction();

	private SaveProjectAction saveProjectAction = new SaveProjectAction();
	private OpenProjectAction openProjectAction = new OpenProjectAction();

	public NewTreeNode getNewTreeNode() {
		return newTreeNode;
	}

	public void setNewTreeNode(NewTreeNode newTreeNode) {
		this.newTreeNode = newTreeNode;
	}

	public Cascade getCascade() {
		return cascade;
	}

	public void setCascade(Cascade cascade) {
		this.cascade = cascade;
	}

	public TileH getTileH() {
		return tileH;
	}

	public void setTileH(TileH tileH) {
		this.tileH = tileH;
	}

	public TileV getTileV() {
		return tileV;
	}

	public void setTileV(TileV tileV) {
		this.tileV = tileV;
	}

	public CircleAction getCircleAction() {
		return circleAction;
	}

	public void setCircleAction(CircleAction circleAction) {
		this.circleAction = circleAction;
	}

	public RectangleAction getRectangleAction() {
		return rectangleAction;
	}

	public void setRectangleAction(RectangleAction rectangleAction) {
		this.rectangleAction = rectangleAction;
	}

	public SelectAction getSelectAction() {
		return selectAction;
	}

	public void setSelectAction(SelectAction selectAction) {
		this.selectAction = selectAction;
	}

	public TriangleAction getTriangleAction() {
		return triangleAction;
	}

	public void setTriangleAction(TriangleAction triangleAction) {
		this.triangleAction = triangleAction;
	}

	public DeleteElement getDeleteElement() {
		return deleteElement;
	}

	public void setDeleteElement(DeleteElement deleteElement) {
		this.deleteElement = deleteElement;
	}

	public ZoomIn getZoomIn() {
		return zoomIn;
	}

	public void setZoomIn(ZoomIn zoomIn) {
		this.zoomIn = zoomIn;
	}

	public ZoomOut getZoomOut() {
		return zoomOut;
	}

	public void setZoomOut(ZoomOut zoomOut) {
		this.zoomOut = zoomOut;
	}

	public ZoomToFit getZoomToFit() {
		return zoomToFit;
	}

	public void setZoomToFit(ZoomToFit zoomToFit) {
		this.zoomToFit = zoomToFit;
	}

	public DefaultSlotNode getDefaultSlotNode() {
		return defaultSlotNode;
	}

	public void setDefaultSlotNode(DefaultSlotNode defaultSlotNode) {
		this.defaultSlotNode = defaultSlotNode;
	}

	public LinkAction getLinkAction() {
		return linkAction;
	}

	public void setLinkAction(LinkAction linkAction) {
		this.linkAction = linkAction;
	}

	public DeleteTreeNode getDeleteTreeNode() {
		return deleteTreeNode;
	}

	public void setDeleteTreeNode(DeleteTreeNode deleteTreeNode) {
		this.deleteTreeNode = deleteTreeNode;
	}

	public RedoAction getRedoAction() {
		return redoAction;
	}

	public void setRedoAction(RedoAction redoAction) {
		this.redoAction = redoAction;
	}

	public UndoAction getUndoAction() {
		return undoAction;
	}

	public void setUndoAction(UndoAction undoAction) {
		this.undoAction = undoAction;
	}

	public TextAction getTextAction() {
		return textAction;
	}

	public void setTextAction(TextAction textAction) {
		this.textAction = textAction;
	}

	public CopyAction getCopyAction() {
		return copyAction;
	}

	public void setCopyAction(CopyAction copyAction) {
		this.copyAction = copyAction;
	}

	public PasteAction getPasteAction() {
		return pasteAction;
	}

	public void setPasteAction(PasteAction pasteAction) {
		this.pasteAction = pasteAction;
	}

	public CutAction getCutAction() {
		return cutAction;
	}

	public void setCutAction(CutAction cutAction) {
		this.cutAction = cutAction;
	}

	public SelectAllAction getSelectAllAction() {
		return selectAllAction;
	}

	public void setSelectAllAction(SelectAllAction selectAllAction) {
		this.selectAllAction = selectAllAction;
	}

	public SaveProjectAction getSaveProjectAction() {
		return saveProjectAction;
	}

	public void setSaveProjectAction(SaveProjectAction saveProjectAction) {
		this.saveProjectAction = saveProjectAction;
	}

	public OpenProjectAction getOpenProjectAction() {
		return openProjectAction;
	}

	public void setOpenProjectAction(OpenProjectAction openProjectAction) {
		this.openProjectAction = openProjectAction;
	}

	public LinkSlots getLinkSlots() {
		return linkSlots;
	}

	public void setLinkSlots(LinkSlots linkSlots) {
		this.linkSlots = linkSlots;
	}

}
