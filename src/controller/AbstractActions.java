package controller;

import java.net.URL;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public abstract class AbstractActions extends AbstractAction {

	private static final long serialVersionUID = 1L;

	// UÄ�itava ikonice zadate iz package "icons" pomoÄ‡u zadate putanje do fajla.

	public Icon loadIcon(String fileName) {
		URL imageURL = getClass().getResource(fileName);
		Icon icon = null;

		if (imageURL != null) {
			icon = new ImageIcon(imageURL);

			// U sluÄ�aju da doÄ‘e do toga da ne moÅ¾e da pronaÄ‘e zadatu ikonicu
			// na odreÄ‘enoj putanji ispisujemo error "Resource not found" pa fileName.
		} else {
			System.err.println("Resource not found: " + fileName);
		}

		return icon;
	}

}
