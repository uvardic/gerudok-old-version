package controller.treeListeners;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import controller.AbstractActions;
import core.MainFrame;
import tree.nodes.DiagramNode;
import tree.nodes.ElementNode;
import tree.nodes.FolderNode;
import tree.nodes.PageNode;
import tree.nodes.ProjectNode;
import tree.nodes.SlotNode;
import tree.nodes.WorkspaceNode;
import view.Diagram;
import view.Page;
import view.Slot;

public class DeleteTreeNode extends AbstractActions {

	private static final long serialVersionUID = 1L;

	public DeleteTreeNode() {
		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_D, ActionEvent.SHIFT_MASK));
		putValue(Action.SMALL_ICON, loadIcon("/icons/menuIcons/DeleteIcon.png"));
		putValue(Action.NAME, "Delete Node");
		putValue(Action.SHORT_DESCRIPTION, "Delete Node");
	}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			Object o = MainFrame.getInstance().getTree().getLastSelectedPathComponent();
	
			if (o instanceof ProjectNode) {
				ProjectNode projectNode = (ProjectNode) o;
				for (DiagramNode diagramNode : projectNode.getDiagrams()) {
					for (Diagram diagram : MainFrame.getInstance().getDiagrams()) {
						if (diagram.getName().equals(diagramNode.getName())) {
							diagram.dispose();
						}
					}
				}
	
				((WorkspaceNode) MainFrame.getInstance().getTree().getModel().getRoot()).getProjects().remove(projectNode);
			}
	
			else if (o instanceof DiagramNode) {
				DiagramNode diagramNode = (DiagramNode) o;
				for (Diagram diagram : MainFrame.getInstance().getDiagrams()) {
					if (diagram.getName().equals(diagramNode.getName())) {
						diagram.dispose();
					}
				}
	
				((ProjectNode) diagramNode.getParent()).getDiagrams().remove(diagramNode);
			}
	
			else if (o instanceof PageNode) {
				PageNode pageNode = (PageNode) o;
				for (Diagram diagram : MainFrame.getInstance().getDiagrams()) {
					for (Page page : diagram.getPages()) {
						if (page.getName().equals(pageNode.getName())) {
							diagram.getTabbedPane().remove(page);
						}
					}
				}
	
				((DiagramNode) pageNode.getParent()).getPages().remove(pageNode);
			}
	
			else if (o instanceof SlotNode) {
				SlotNode slotNode = (SlotNode) o;
				for (Diagram diagram : MainFrame.getInstance().getDiagrams()) {
					for (Page page : diagram.getPages()) {
						for (Slot slot : page.getSlots()) {
							if (slot.getName().equals(slotNode.getName())) {
								page.getTabbedPane().remove(slot);
							}
						}
					}
				}
	
				((PageNode) slotNode.getParent()).getSlots().remove(slotNode);
			}
	
			else if (o instanceof ElementNode) {
				ElementNode elementNode = (ElementNode) o;
				MainFrame.getInstance().getActionManager().getDeleteElement().actionPerformed(arg0);
	
				((FolderNode) elementNode.getParent()).removeElementNode(elementNode);
			}
	
			SwingUtilities.updateComponentTreeUI(MainFrame.getInstance().getTree());
		}

}
