package controller.treeListeners;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;

import javax.swing.Action;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import controller.AbstractActions;
import core.MainFrame;
import tree.nodes.DiagramNode;
import tree.nodes.PageNode;
import tree.nodes.ProjectNode;
import tree.nodes.SlotNode;
import tree.nodes.WorkspaceNode;
import view.Diagram;
import view.Page;
import view.Slot;

public class NewTreeNode extends AbstractActions {

	private static final long serialVersionUID = 1L;

	public NewTreeNode() {
		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.CTRL_MASK));
		putValue(Action.NAME, "New Tree Node");
		putValue(Action.SHORT_DESCRIPTION, "New Tree Node");
		putValue(Action.SMALL_ICON, loadIcon("/icons/treeIcons/ProjectIcon.png"));
	}

		@Override
		public void actionPerformed(ActionEvent e) {
			Object o = MainFrame.getInstance().getTree().getLastSelectedPathComponent();
	
			if (o == null || o instanceof WorkspaceNode) {
				ProjectNode projectNode = new ProjectNode("Project");
				DiagramNode diagramNode = new DiagramNode("Diagram");
	
				Diagram diagram = new Diagram();
	
				MainFrame.getInstance().getTree().addProjectNode(projectNode);
				projectNode.addDiagramNode(diagramNode);
	
				diagram.setDiagramNode(diagramNode);
	
				MainFrame.getInstance().getDesktopPane().add(diagram);
				MainFrame.getInstance().getDiagrams().add(diagram);
	
				try {
					diagram.setSelected(true);
				} catch (PropertyVetoException e1) {
					e1.printStackTrace();
				}
			}
	
			else if (o instanceof ProjectNode) {
				ProjectNode projectNode = (ProjectNode) o;
				DiagramNode diagramNode = new DiagramNode("Diagram");
	
				Diagram diagram = new Diagram();
	
				projectNode.addDiagramNode(diagramNode);
	
				diagram.setDiagramNode(diagramNode);
	
				MainFrame.getInstance().getDesktopPane().add(diagram);
				MainFrame.getInstance().getDiagrams().add(diagram);
	
				try {
					diagram.setSelected(true);
				} catch (PropertyVetoException e1) {
					e1.printStackTrace();
				}
			}
	
			else if (o instanceof DiagramNode) {
				DiagramNode diagramNode = (DiagramNode) o;
				PageNode pageNode = new PageNode("Page");
	
				Diagram diagram = (Diagram) MainFrame.getInstance().getDesktopPane().getSelectedFrame();
				Page page = new Page();
	
				diagramNode.addPageNode(pageNode);
	
				page.setPageNode(pageNode);
	
				diagram.addPage(page);
	
				diagram.setSelectedPage(page);
			}
	
			else if (o instanceof PageNode) {
				PageNode pageNode = (PageNode) o;
				SlotNode slotNode = new SlotNode("Slot");
	
				Diagram diagram = (Diagram) MainFrame.getInstance().getDesktopPane().getSelectedFrame();
				Page page = diagram.getSelectedPage();
				Slot slot = new Slot();
	
				pageNode.addSlotNode(slotNode);
	
				slot.setSlotNode(slotNode);
	
				page.addSlot(slot);
	
				page.setSelectedSlot(slot);
			}

		SwingUtilities.updateComponentTreeUI(MainFrame.getInstance().getTree());

	}

}
