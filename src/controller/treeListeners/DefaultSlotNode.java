package controller.treeListeners;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;

import javax.swing.Action;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import controller.AbstractActions;
import core.MainFrame;
import tree.nodes.DiagramNode;
import tree.nodes.PageNode;
import tree.nodes.ProjectNode;
import tree.nodes.SlotNode;
import view.Diagram;
import view.Page;
import view.Slot;

public class DefaultSlotNode extends AbstractActions {

	private static final long serialVersionUID = 1L;

	public DefaultSlotNode() {
		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		putValue(Action.NAME, "Default Slot Node");
		putValue(Action.SHORT_DESCRIPTION, "Default Slot Node");
		putValue(Action.SMALL_ICON, loadIcon("/icons/treeIcons/SlothIcon.png"));
	}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			ProjectNode projectNode = new ProjectNode("Project");
			DiagramNode diagramNode = new DiagramNode("Diagram");
			PageNode pageNode = new PageNode("Page");
			SlotNode slotNode = new SlotNode("Slot");
	
			Diagram diagram = new Diagram();
			Page page = new Page();
			Slot slot = new Slot();
	
			MainFrame.getInstance().getTree().addProjectNode(projectNode);
			projectNode.addDiagramNode(diagramNode);
			diagramNode.addPageNode(pageNode);
			pageNode.addSlotNode(slotNode);
	
			diagram.setDiagramNode(diagramNode);
			page.setPageNode(pageNode);
			slot.setSlotNode(slotNode);
	
			MainFrame.getInstance().getDesktopPane().add(diagram);
			MainFrame.getInstance().getDiagrams().add(diagram);
			diagram.addPage(page);
			page.addSlot(slot);
	
			try {
				diagram.setSelected(true);
				diagram.setSelectedPage(page);
				page.setSelectedSlot(slot);
			} catch (PropertyVetoException e1) {
				e1.printStackTrace();
			}
	
			SwingUtilities.updateComponentTreeUI(MainFrame.getInstance().getTree());
		}

}
