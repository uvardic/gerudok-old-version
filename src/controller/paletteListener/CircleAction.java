package controller.paletteListener;

import java.awt.event.ActionEvent;

import javax.swing.Action;

import controller.AbstractActions;
import core.MainFrame;
import view.Diagram;
import view.Page;
import view.Slot;
import view.gui.Dialog;

public class CircleAction extends AbstractActions {

	private static final long serialVersionUID = 1L;

	
	// IzvrÅ¡ava se akcija i na odreÄ‘enom slotu se iscrtava oblik koji selektujemo
	// u ovom sluÄ�aju je krug.
	// Ova klasa takoÄ‘e nasleÄ‘uje "AbstractActions" abstraktnu klasu koja
	// omoguÄ‡ava
	// unos akcije.

	
	public CircleAction() {
		putValue(Action.NAME, "Circle State");
		putValue(Action.SHORT_DESCRIPTION, "Circle State");
		putValue(Action.SMALL_ICON, loadIcon("/icons/paletteIcons/CircleIcon.png"));
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Diagram diagram = MainFrame.getInstance().getSelectedDiagram();
		if (diagram == null) {
			Dialog.message("Please open a diagram first!");
			return;
		}

		Page page = diagram.getSelectedPage();
		if (page == null) {
			Dialog.message("Please open a page first!");
			return;
		}

		Slot slot = page.getSelectedSlot();
		if (slot == null) {
			Dialog.message("Please open a slot first");
			return;
		}

		slot.startCircleState();
		MainFrame.getInstance().getStatusBar().setState("Current State: Circle");
	}
}
