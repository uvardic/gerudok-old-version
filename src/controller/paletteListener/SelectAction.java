package controller.paletteListener;

import java.awt.event.ActionEvent;

import javax.swing.Action;

import controller.AbstractActions;
import core.MainFrame;
import view.Diagram;
import view.Page;
import view.Slot;
import view.gui.Dialog;

public class SelectAction extends AbstractActions {

	private static final long serialVersionUID = 1L;

	public SelectAction() {
		putValue(Action.NAME, "Selection State");
		putValue(Action.SHORT_DESCRIPTION, "Selection State");
		putValue(Action.SMALL_ICON, loadIcon("/icons/paletteIcons/SelectIcon.png"));
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Diagram diagram = MainFrame.getInstance().getSelectedDiagram();
		if (diagram == null) {
			Dialog.message("Please open a diagram first!");
			return;
		}

		Page page = diagram.getSelectedPage();
		if (page == null) {
			Dialog.message("Please open a page first!");
			return;
		}

		Slot slot = page.getSelectedSlot();
		if (slot == null) {
			Dialog.message("Please open a slot first");
			return;
		}

		slot.startSelectionState();
		MainFrame.getInstance().getStatusBar().setState("Current State: Select");
	}

}
