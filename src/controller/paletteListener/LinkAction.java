package controller.paletteListener;

import java.awt.event.ActionEvent;

import javax.swing.Action;

import controller.AbstractActions;
import core.MainFrame;
import view.Diagram;
import view.Page;
import view.Slot;
import view.gui.Dialog;

public class LinkAction extends AbstractActions {

	private static final long serialVersionUID = 1L;

	public LinkAction() {
		putValue(Action.NAME, "Link State");
		putValue(Action.SHORT_DESCRIPTION, "Link State");
		putValue(Action.SMALL_ICON, loadIcon("/icons/paletteIcons/LinkIcon.png"));
	}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			Diagram diagram = MainFrame.getInstance().getSelectedDiagram();
			if (diagram == null) {
				
				Dialog.message("Please open a diagram first!");
				return;
			}
	
			Page page = diagram.getSelectedPage();
			if (page == null) {
				Dialog.message("Please open a page first!");
				return;
			}
	
			Slot slot = page.getSelectedSlot();
			if (slot == null) {
				Dialog.message("Please open a slot first");
				return;
			}
	
			slot.startLinkState();
			MainFrame.getInstance().getStatusBar().setState("Current State: Link");
	}

}
