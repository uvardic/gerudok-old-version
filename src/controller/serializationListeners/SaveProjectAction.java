package controller.serializationListeners;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.KeyStroke;

import controller.AbstractActions;
import core.MainFrame;
import tree.nodes.ProjectNode;
import view.Dialog;

public class SaveProjectAction extends AbstractActions {

	private static final long serialVersionUID = 1L;

	public SaveProjectAction() {
		putValue(Action.NAME, "Save Project");
		putValue(Action.SHORT_DESCRIPTION, "Save Project");
		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		putValue(Action.SMALL_ICON, loadIcon("/icons/menuIcons/SaveIcon.png"));
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new ProjectFileFilter());

			ProjectNode projectNode = MainFrame.getInstance().getTree().getCurrentProjectNode();
			if (projectNode == null) {
				Dialog.message("Select a project node first!");
				return;
			}
			File projectFile = projectNode.getProjectFile();
	
			if (!projectNode.isChanged()) {
				Dialog.message("No changes have been made!");
				return;
			}
	
			if (projectNode.getProjectFile() == null) {
				if (fileChooser.showSaveDialog(MainFrame.getInstance()) == JFileChooser.APPROVE_OPTION) {
					String filePath = fileChooser.getSelectedFile().getPath() + ".gpf";
					projectFile = new File(filePath);
				} else {
					return;
				}
			}

		ObjectOutputStream out;
		try {
			out = new ObjectOutputStream(new FileOutputStream(projectFile));
			out.writeObject(projectNode);
			projectNode.setProjectFile(projectFile);
			projectNode.setChanged(false);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
