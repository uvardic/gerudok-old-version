package controller.serializationListeners;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import controller.AbstractActions;
import core.MainFrame;
import tree.nodes.DiagramNode;
import tree.nodes.PageNode;
import tree.nodes.ProjectNode;
import tree.nodes.SlotNode;
import view.Diagram;
import view.Dialog;
import view.Page;
import view.Slot;

public class OpenProjectAction extends AbstractActions {

	private static final long serialVersionUID = 1L;

	public OpenProjectAction() {
		putValue(Action.NAME, "Open Project");
		putValue(Action.SHORT_DESCRIPTION, "Open Project");
		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.SHIFT_MASK));
		putValue(Action.SMALL_ICON, loadIcon("/icons/menuIcons/OpenIcon.png"));
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new ProjectFileFilter());

		if (fileChooser.showOpenDialog(MainFrame.getInstance()) == JFileChooser.APPROVE_OPTION) {
			ObjectInputStream in = null;
			try {
					in = new ObjectInputStream(new FileInputStream(fileChooser.getSelectedFile()));
	
					ProjectNode projectNode = null;
					try {
						projectNode = (ProjectNode) in.readObject();
					} catch (ClassNotFoundException e) {
						Dialog.message("Wrong file!");
						e.printStackTrace();
					}

					MainFrame.getInstance().getTree().addProjectNode(projectNode);
	
					for (int i = 0; i < projectNode.getDiagrams().size(); i++) {
						Diagram diagram = new Diagram();
						DiagramNode diagramNode = projectNode.getDiagrams().get(i);
						diagram.setDiagramNode(diagramNode);
	
						MainFrame.getInstance().addDiagram(diagram);
						diagram.setSelected(true);
	
						for (int j = 0; j < diagramNode.getPages().size(); j++) {
							Page page = new Page();
							PageNode pageNode = diagramNode.getPages().get(j);
							page.setPageNode(pageNode);
	
							diagram.addPage(page);
	
							for (int k = 0; k < pageNode.getSlots().size(); k++) {
								Slot slot = new Slot();
								SlotNode slotNode = pageNode.getSlots().get(k);
								slotNode.getSlotModel().addUpdateListener(projectNode);
								slot.setSlotNode(slotNode);
	
								page.addSlot(slot);
							}
						}
					}

				SwingUtilities.updateComponentTreeUI(MainFrame.getInstance().getTree());
				
			} catch (IOException | PropertyVetoException e) {
				
				e.printStackTrace();
				
			}
			
		}
		
		
	}

	
}
