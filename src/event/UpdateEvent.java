package event;

import java.util.EventObject;

public class UpdateEvent extends EventObject {

	private static final long serialVersionUID = 1L;

	public UpdateEvent(Object source) {
		super(source);
	}

}
