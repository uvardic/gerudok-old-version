package view;

import java.awt.Adjustable;
import java.awt.BorderLayout;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.ArrayList;

import javax.swing.JInternalFrame;
import javax.swing.JScrollBar;
import javax.swing.JTabbedPane;
import javax.swing.WindowConstants;

import tree.nodes.DiagramNode;
import utilities.Constants;

public class Diagram extends JInternalFrame implements AdjustmentListener {

	private static final long serialVersionUID = 1L;

	public static int openFrames = 0;

	private DiagramNode diagramNode;
	private JTabbedPane tabbedPane = new JTabbedPane();

	private JScrollBar verticalScroll = new JScrollBar(JScrollBar.VERTICAL, 140, 20, 0, 300);
	private JScrollBar horizontalScroll = new JScrollBar(JScrollBar.HORIZONTAL, 140, 20, 0, 300);
	// pocetne pozicije scroll barova
	private int verticalScrollValue = 140;
	private int horizontalScrollValue = 140;

	private ArrayList<Page> pages = new ArrayList<>();

	public Diagram() {
		super("", true, true, true, true); // ime, resizable, closable, maximizable, iconifiable

		openFrames++;

		setSize(Constants.WIDTH / 2, Constants.HEIGHT / 2);
		// na ovaj nacin novi prozor se otvara "smaknut u odnosu na prethodni"
		setLocation(openFrames * Constants.xOFFSET, openFrames * Constants.yOFFSET);
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);

		verticalScroll.addAdjustmentListener(this);
		horizontalScroll.addAdjustmentListener(this);

		add(tabbedPane, BorderLayout.CENTER);
		add(horizontalScroll, BorderLayout.SOUTH);
		add(verticalScroll, BorderLayout.EAST);
		setVisible(true);
	}

	public void addPage(Page page) {
		pages.add(page);
		tabbedPane.addTab(page.getName(), page);
	}

	// Povezivanje sa granom
	public void setDiagramNode(DiagramNode diagramNode) {
		this.diagramNode = diagramNode;
		setName(diagramNode.getName());
		setTitle(diagramNode.getName());
		updateUI();
	}

	// ----------------------SCROLL----------------------------------------------------------------

	@Override
	public void adjustmentValueChanged(AdjustmentEvent e) {
		Page page = (Page) tabbedPane.getSelectedComponent();
		Slot slot = (Slot) page.getTabbedPane().getSelectedComponent();

		if (e.getAdjustable().getOrientation() == Adjustable.HORIZONTAL) {
			slot.getTransformation().translate((e.getValue() - horizontalScrollValue) * (-Constants.TRANSLATE_FACTOR),
					0);
			horizontalScrollValue = e.getValue();
		} else {
			slot.getTransformation().translate(0, (e.getValue() - verticalScrollValue) * (-Constants.TRANSLATE_FACTOR));
			verticalScrollValue = e.getValue();
		}

		repaint();
	}

	public void adjustScrollBars(int vertical, int horizontal) {
		if (((verticalScroll.getValue() >= verticalScroll.getMinimum()) && (vertical > 0))
				|| ((vertical < 0) && (verticalScroll.getValue() <= verticalScroll.getMaximum()))) {
			verticalScroll.setValue(verticalScroll.getValue() + verticalScroll.getUnitIncrement() * vertical);
		}

		if (((horizontalScroll.getValue() >= horizontalScroll.getMinimum()) && (horizontal > 0))
				|| ((horizontal < 0) && (horizontalScroll.getValue() <= horizontalScroll.getMaximum()))) {
			horizontalScroll.setValue(horizontalScroll.getValue() + horizontalScroll.getUnitIncrement() * horizontal);
		}
	}

	// ----------------------GETTERS---------------------------------------------------------------

	public Page getSelectedPage() {
		return (Page) tabbedPane.getSelectedComponent();
	}

	public void setSelectedPage(Page page) {
		tabbedPane.setSelectedComponent(page);
	}

	public DiagramNode getDiagramNode() {
		return diagramNode;
	}

	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	public void setTabbedPane(JTabbedPane tabbedPane) {
		this.tabbedPane = tabbedPane;
	}

	public ArrayList<Page> getPages() {
		return pages;
	}

	public void setPages(ArrayList<Page> pages) {
		this.pages = pages;
	}

}
