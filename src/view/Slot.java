package view;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.datatransfer.Transferable;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import commands.CommandManager;
import core.MainFrame;
import event.UpdateEvent;
import event.UpdateListener;
import model.SlotElementsSelection;
import model.elements.SlotElement;
import model.elements.deviceElements.SlotDevice;
import model.elements.linkElements.LinkElement;
import painters.ElementPainter;
import painters.InputOutputPainter;
import state.StateManager;
import tree.nodes.ElementNode;
import tree.nodes.SlotNode;
import utilities.Constants;

public class Slot extends JPanel implements UpdateListener {

	private static final long serialVersionUID = 1L;

	public static int openSlots = 0;

	private SlotNode slotNode;
	private JPanel framework = new Framework();

	private StateManager stateManager = new StateManager(this);
	private CommandManager commandManager = new CommandManager();

	// Tacka koja sluzi za lasso state
	private Point2D lastPoint = null;
	private Rectangle2D selectionRectangle = new Rectangle2D.Double();

	private AffineTransform transformation = new AffineTransform();

	public Slot() {
		openSlots++;

		setLayout(new BorderLayout());

		SlotController slotController = new SlotController();

		framework.addMouseListener(slotController);
		framework.addMouseMotionListener(slotController);
		framework.addMouseWheelListener(slotController);
		framework.setCursor(new Cursor(Cursor.HAND_CURSOR));
		framework.setBackground(Color.WHITE);
		add(framework, BorderLayout.CENTER);
	}

	public void setSlotNode(SlotNode slotNode) {
		this.slotNode = slotNode;
		this.slotNode.getSlotModel().addUpdateListener(this);
		this.slotNode.getSelectionModel().addUpdateListener(this);
		setName(slotNode.getName());
	}

	// ----------------------SLOT_CONTROLLER-------------------------------------------------------

	private class SlotController extends MouseAdapter {

		@Override
		public void mousePressed(MouseEvent e) {
			lastPoint = e.getPoint();
			transformToUserSpace(lastPoint);
			stateManager.getCurrentState().mousePressed(e);
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			stateManager.getCurrentState().mouseReleased(e);
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			stateManager.getCurrentState().mouseDragged(e);
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			stateManager.getCurrentState().mouseMoved(e);
			MainFrame.getInstance().getStatusBar().setPoint("Current Point: x = " + e.getX() + " y = " + e.getY());
		}

		@Override
		public void mouseWheelMoved(MouseWheelEvent e) {
			Diagram diagram = (Diagram) MainFrame.getInstance().getDesktopPane().getSelectedFrame();

			if ((e.getModifiers() & InputEvent.CTRL_MASK) != 0) {
				// Odredimo novo skaliranje
				double newScaling = transformation.getScaleX();

				// ima negativnu vrednost ako se tockic rotira na gore/od korisnika,
				// a pozitivnu vrednost ako se tockic rotira na dole/ka korisniku
				if (e.getWheelRotation() > 0) {
					newScaling /= e.getWheelRotation() * Constants.SCALING_FACTOR;
				} else {
					newScaling *= -(double) e.getWheelRotation() * Constants.SCALING_FACTOR;
				}

				// Odrzavamo skaliranje u intervalu od 0,2 do 5
				if (newScaling < 0.2) {
					newScaling = 0.2;
				}

				if (newScaling > 5) {
					newScaling = 5;
				}

				Point2D oldPoint = e.getPoint();
				transformToUserSpace(oldPoint);
				transformation.setToScale(newScaling, newScaling);

				Point2D newPoint = e.getPoint();
				transformToUserSpace(newPoint);
				transformation.translate(newPoint.getX() - oldPoint.getX(), newPoint.getY() - oldPoint.getY());
			} else if ((e.getModifiers() & InputEvent.SHIFT_MASK) != 0) {
				diagram.adjustScrollBars(0, e.getWheelRotation());
			} else {
				diagram.adjustScrollBars(e.getWheelRotation(), 0);
			}

			repaint();
		}

	}

	@Override
	public void updatePerformed(UpdateEvent e) {
		repaint();
	}

	// -----------------------FRAMEWORK------------------------------------------------------------

	private class Framework extends JPanel {

		private static final long serialVersionUID = 1L;

		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);

			Graphics2D g2 = (Graphics2D) g;
			// U slucaju preklapanja
			g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.8f));
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

			// Transformacija
			g2.transform(transformation);

			// Iscrtavanje elemenata
			Iterator<SlotElement> it = slotNode.getSlotModel().getElements().iterator();
			while (it.hasNext()) {
				SlotElement element = it.next();
				ElementPainter painter = element.getElementPainter();
				painter.paint(g2, element);
			}

			// Iscrtavanje hendlova
			paintSelectionHandles(g2);

			// Iscrtavanje pravugaonika za lasso
			g2.setColor(Color.BLACK);
			g2.setStroke(
					new BasicStroke(1, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_BEVEL, 1f, new float[] { 3, 6 }, 0));
			g2.draw(selectionRectangle);
		}
	}

	// ----------------------HANDLE----------------------------------------------------------------

	// Iscrtavanje selekcionih hendlova oko selektovanog elementa
	private void paintSelectionHandles(Graphics2D g2) {
		Iterator<SlotElement> it = slotNode.getSelectionModel().getSelectionList().listIterator();
		while (it.hasNext()) {
			SlotElement element = it.next();

			if (element instanceof SlotDevice) {
				SlotDevice device = (SlotDevice) element;

				// Isprekidana linija
				g2.setStroke(new BasicStroke(1, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_BEVEL, 1f,
						new float[] { 3f, 6f }, 0));
				g2.setPaint(Color.BLACK);

				g2.drawRect((int) device.getPointX(), (int) device.getPointY(), (int) device.getWidth(),
						(int) device.getHeight());

				for (Handle handle : Handle.values()) {
					paintSelectionHandle(g2, getHandlePoint(device.getPoint(), device.getSize(), handle));
				}

			} else {
				LinkElement link = (LinkElement) element;
				Point2D point = null;

				point = link.getOutput().getPoint();
				g2.setPaint(Color.BLACK);
				g2.setStroke(new BasicStroke(2, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_BEVEL));

				g2.fillRect((int) point.getX() - Constants.HANDLE_SIZE / 2,
						(int) point.getY() - Constants.HANDLE_SIZE / 2, Constants.HANDLE_SIZE, Constants.HANDLE_SIZE);

				Iterator<Point2D> pointIterator = link.getPoints().iterator();
				while (pointIterator.hasNext()) {
					point = pointIterator.next();
					g2.fillRect((int) point.getX() - Constants.HANDLE_SIZE / 2,
							(int) point.getY() - Constants.HANDLE_SIZE / 2, Constants.HANDLE_SIZE,
							Constants.HANDLE_SIZE);
				}

				point = link.getInput().getPoint();
				g2.fillRect((int) point.getX() - Constants.HANDLE_SIZE / 2,
						(int) point.getY() - Constants.HANDLE_SIZE / 2, Constants.HANDLE_SIZE, Constants.HANDLE_SIZE);
			}
		}
	}

	// Iscrtavanje hendla
	private void paintSelectionHandle(Graphics2D g2, Point2D point) {
		double size = Constants.HANDLE_SIZE;
		g2.fill(new Rectangle2D.Double(point.getX() - size / 2, point.getY() - size / 2, size, size));
	}

	// Odredjivanje pozicije hendla
	private Point2D getHandlePoint(Point2D topLeft, Dimension size, Handle handle) {
		double x = 0;
		double y = 0;

		if (handle == Handle.NORTH_WEST || handle == Handle.NORTH || handle == Handle.NORTH_EAST) {
			y = topLeft.getY();
		}

		if (handle == Handle.EAST || handle == Handle.WEST) {
			y = topLeft.getY() + size.getHeight() / 2;
		}

		if (handle == Handle.SOUTH_WEST || handle == Handle.SOUTH || handle == Handle.SOUTH_EAST) {
			y = topLeft.getY() + size.getHeight();
		}

		if (handle == Handle.NORTH_WEST || handle == Handle.WEST || handle == Handle.SOUTH_WEST) {
			x = topLeft.getX();
		}

		if (handle == Handle.NORTH || handle == Handle.SOUTH) {
			x = topLeft.getX() + size.getWidth() / 2;
		}

		if (handle == Handle.NORTH_EAST || handle == Handle.EAST || handle == Handle.SOUTH_EAST) {
			x = topLeft.getX() + size.getWidth();
		}

		return new Point2D.Double(x, y);
	}

	// Postavljanje misa u odnosu na hendl
	public void setMouseCursor(Point2D point) {
		Handle handle = getDeviceAndHandleForPoint(point);

		if (handle != null) {
			Cursor cursor = null;

			switch (handle) {
			case NORTH:
				cursor = Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR);
				break;
			case SOUTH:
				cursor = Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR);
				break;
			case EAST:
				cursor = Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR);
				break;
			case WEST:
				cursor = Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR);
				break;
			case NORTH_EAST:
				cursor = Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR);
				break;
			case NORTH_WEST:
				cursor = Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR);
				break;
			case SOUTH_EAST:
				cursor = Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR);
				break;
			case SOUTH_WEST:
				cursor = Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR);
				break;
			}
			framework.setCursor(cursor);
		} else {
			framework.setCursor(Cursor.getDefaultCursor());
		}
	}

	// Odredjuje handl i uredjaj koji se nalazi na zadatoj lokaciji
	public Handle getDeviceAndHandleForPoint(Point2D point) {
		SlotElement element;

		Iterator<SlotElement> it = slotNode.getSelectionModel().getSelectionList().iterator();
		while (it.hasNext()) {
			element = it.next();

			return getHandleForPoint(element, point);
		}

		return null;
	}

	// Za zadatku tacku i uredjaj vraca hendl
	private Handle getHandleForPoint(SlotElement element, Point2D point) {
		for (Handle handle : Handle.values()) {
			if (isPointInHandle(element, point, handle)) {
				return handle;
			}
		}
		return null;
	}

	// Za zadati ure�aj, ta�ku i hendl odre�uje da li je ta�ka unutar hendla
	private boolean isPointInHandle(SlotElement element, Point2D point, Handle handle) {
		if (element instanceof SlotDevice) {
			SlotDevice device = (SlotDevice) element;
			Point2D handleCenter = getHandlePoint(device.getPoint(), device.getSize(), handle);
			return ((Math.abs(point.getX() - handleCenter.getX()) <= (double) Constants.HANDLE_SIZE / 2)
					&& (Math.abs(point.getY() - handleCenter.getY()) <= (double) Constants.HANDLE_SIZE / 2));
		} else {
			return false;
		}
	}

	// ----------------------TRANSFORMATION--------------------------------------------------------

	public void transformToUserSpace(Point2D deviceSpace) {
		try {
			transformation.inverseTransform(deviceSpace, deviceSpace);
		} catch (NoninvertibleTransformException e) {
			e.printStackTrace();
		}
	}

	// Zoom na cetar, koristi se kod akcija ZoomIn i ZoomOut
	// direction - smer zumiranja
	public void centerZoom(boolean direction) {
		double newScaling = transformation.getScaleX();

		if (newScaling < 0.2) {
			newScaling = 0.2;
		}
		if (newScaling > 5) {
			newScaling = 5;
		}

		if (direction == true) {
			newScaling *= Constants.SCALING_FACTOR;
		} else {
			newScaling /= Constants.SCALING_FACTOR;
		}

		Point2D oldPoint = new Point(getWidth() / 2, getHeight() / 2);
		transformToUserSpace(oldPoint);
		transformation.setToScale(newScaling, newScaling);

		Point2D newPoint = new Point(getWidth() / 2, getHeight() / 2);
		transformToUserSpace(newPoint);
		transformation.translate(newPoint.getX() - oldPoint.getX(), newPoint.getY() - oldPoint.getY());

		repaint();
	}

	public double transformLineToUserSpaceX(double deviceSpace) {
		return deviceSpace * transformation.getScaleX() + transformation.getTranslateX();
	}

	public double transformLineToUserSpaceY(double deviceSpace) {
		return deviceSpace * transformation.getScaleY() + transformation.getTranslateY();
	}

	// Zoom nad oznacenim regionom
	// minX - leva granica
	// maxX - desna granica
	// minY - gornja granica
	// maxY - donja granica
	public void regionZoom(double minX, double maxX, double minY, double maxY) {
		double minXX, maxXX, minYY, maxYY;
		maxXX = transformLineToUserSpaceX(maxX);
		minXX = transformLineToUserSpaceX(minX);
		maxYY = transformLineToUserSpaceY(maxY);
		minYY = transformLineToUserSpaceY(minY);

		double regionWidth = maxXX - minXX;
		double regionHeight = maxYY - minYY;
		int deviceWidth = this.getWidth();
		int deviceHeight = this.getHeight();
		double newScaling = transformation.getScaleX();

		if (regionWidth != 0 && regionHeight != 0) {
			double scaleX = (deviceWidth / regionWidth);
			double scaleY = (deviceHeight / regionHeight);

			if (scaleX < scaleY) {
				newScaling *= scaleX;
			} else {
				newScaling *= scaleY;
			}

			if (newScaling < 0.2) {
				newScaling = 0.2;
			}

			if (newScaling > 5) {
				newScaling = 5;
			}

			transformation.setToScale(newScaling, newScaling);
		}

		maxXX = transformLineToUserSpaceX(maxX);
		minXX = transformLineToUserSpaceX(minX);
		maxYY = transformLineToUserSpaceY(maxY);
		minYY = transformLineToUserSpaceY(minY);
		regionWidth = maxXX - minXX;
		regionHeight = maxYY - minYY;
		transformation.translate((-minXX + (deviceWidth - regionWidth) / 2) / transformation.getScaleX(),
				(-minYY + (deviceHeight - regionHeight) / 2) / transformation.getScaleY());
		repaint();
	}

	// Odredjuje granice frameworka u kojima se nalaze elementi
	public void selectionFit() {
		double minX = 0, minY = 0, maxX = 0, maxY = 0;
		Iterator<SlotElement> it = slotNode.getSlotModel().getElements().iterator();
		if (it.hasNext()) {
			SlotElement element = it.next();
			if (element instanceof SlotDevice) {
				SlotDevice device = (SlotDevice) element;
				minX = device.getPoint().getX();
				minY = device.getPoint().getY();
				maxX = device.getPoint().getX() + device.getSize().getWidth();
				maxY = device.getPoint().getY() + device.getSize().getHeight();

				while (it.hasNext()) {
					element = it.next();
					if (element instanceof SlotDevice) {
						device = (SlotDevice) element;
						if (device.getPoint().getX() < minX) {
							minX = device.getPoint().getX();
						}
						if (device.getPoint().getX() + device.getSize().getWidth() > maxX) {
							maxX = device.getPoint().getX() + device.getSize().getWidth();
						}
						if (device.getPoint().getY() < minY) {
							minY = device.getPoint().getY();
						}
						if (device.getPoint().getY() + device.getSize().getHeight() > maxY) {
							maxY = device.getPoint().getY() + device.getSize().getHeight();
						}
					}
				}
			}
		}

		regionZoom(minX, maxX, minY, maxY);
		centerZoom(false);
	}

	// ----------------------CLIPBOARD-------------------------------------------------------------

	@SuppressWarnings("unchecked")
	public void paste() {
		Transferable clipboardContent = MainFrame.getInstance().getClipboard().getContents(MainFrame.getInstance());
		if ((clipboardContent != null)
				&& (clipboardContent.isDataFlavorSupported(SlotElementsSelection.elementFlavor))) {
			try {
				SlotDevice device = null;
				ArrayList<SlotElement> tempElements = (ArrayList<SlotElement>) clipboardContent
						.getTransferData(SlotElementsSelection.elementFlavor);

				for (int i = 0; i < tempElements.size(); i++) {
					if (tempElements.get(i) instanceof SlotDevice) {
						device = (SlotDevice) tempElements.get(i).clone();
						Point2D newLocation = (Point2D) device.getPoint().clone();
						newLocation.setLocation(device.getPointX() + 40, device.getPointY() + 40);
						device.setPoint(newLocation);

						for (int j = 0; j < device.getNumberOfInputs(); j++) {
							Point2D inputPoint = device.getInputAt(j).getPoint();
							inputPoint.setLocation(inputPoint.getX() + 40, inputPoint.getY() + 40);
							device.getInputAt(j).setPoint(inputPoint);
							device.getInputAt(j).setElementPainter(new InputOutputPainter(device.getInputAt(j)));
						}

						for (int j = 0; j < device.getNumberOfOutputs(); j++) {
							Point2D outputPoint = device.getOutputAt(j).getPoint();
							outputPoint.setLocation(outputPoint.getX() + 40, outputPoint.getY() + 40);
							device.getOutputAt(j).setPoint(outputPoint);
							device.getOutputAt(j).setElementPainter(new InputOutputPainter(device.getOutputAt(j)));
						}

						slotNode.getSlotModel().addElement(device);
						new ElementNode(device);
						SwingUtilities.updateComponentTreeUI(MainFrame.getInstance().getTree());
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	// ----------------------MEDIATOR--------------------------------------------------------------

	public void startSelectionState() {
		stateManager.setSelectionState();
	}

	public void startLassoState() {
		stateManager.setLassoState();
	}

	public void startMoveState() {
		stateManager.setMoveState();
	}

	public void startResizeState() {
		stateManager.setResizeState();
	}

	public void startLinkState() {
		slotNode.getSelectionModel().removeAllFromSelectionList();
		stateManager.setLinkState();
	}

	public void startCircleState() {
		slotNode.getSelectionModel().removeAllFromSelectionList();
		stateManager.setCircleState();
	}

	public void startRectangleState() {
		slotNode.getSelectionModel().removeAllFromSelectionList();
		stateManager.setRectangleState();
	}

	public void startTriangleState() {
		slotNode.getSelectionModel().removeAllFromSelectionList();
		stateManager.setTriangleState();
	}

	public void startTextState() {
		stateManager.setTextState();
	}

	// ----------------------GETTERS---------------------------------------------------------------

	public JPanel getFramework() {
		return framework;
	}

	public void setFramework(JPanel framework) {
		this.framework = framework;
	}

	public SlotNode getSlotNode() {
		return slotNode;
	}

	public StateManager getStateManager() {
		return stateManager;
	}

	public void setStateManager(StateManager stateManager) {
		this.stateManager = stateManager;
	}

	public Point2D getLastPoint() {
		return lastPoint;
	}

	public void setLastPoint(Point2D lastPoint) {
		this.lastPoint = lastPoint;
	}

	public Rectangle2D getSelectionRectangle() {
		return selectionRectangle;
	}

	public void setSelectionRectangle(Rectangle2D selectionRectangle) {
		this.selectionRectangle = selectionRectangle;
	}

	public AffineTransform getTransformation() {
		return transformation;
	}

	public void setTransformation(AffineTransform transformation) {
		this.transformation = transformation;
	}

	public CommandManager getCommandManager() {
		return commandManager;
	}

	public void setCommandManager(CommandManager commandManager) {
		this.commandManager = commandManager;
	}

}
