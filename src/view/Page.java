package view;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import tree.nodes.PageNode;

public class Page extends JPanel {

	private static final long serialVersionUID = 1L;

	public static int openPages = 0;

	private PageNode pageNode;
	private JTabbedPane tabbedPane = new JTabbedPane();

	private ArrayList<Slot> slots = new ArrayList<>();

	public Page() {
		openPages++;

		setLayout(new BorderLayout());
		add(tabbedPane, BorderLayout.CENTER);
	}

	public void addSlot(Slot slot) {
		slots.add(slot);
		tabbedPane.addTab(slot.getName(), slot);
	}

	public void setPageNode(PageNode pageNode) {
		this.pageNode = pageNode;
		setName(pageNode.getName());
	}

	// ----------------------GETTERS---------------------------------------------------------------

	public Slot getSelectedSlot() {
		return (Slot) tabbedPane.getSelectedComponent();
	}

	public void setSelectedSlot(Slot slot) {
		tabbedPane.setSelectedComponent(slot);
	}

	public PageNode getPageNode() {
		return pageNode;
	}

	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	public void setTabbedPane(JTabbedPane tabbedPane) {
		this.tabbedPane = tabbedPane;
	}

	public ArrayList<Slot> getSlots() {
		return slots;
	}

	public void setSlots(ArrayList<Slot> slots) {
		this.slots = slots;
	}

}
