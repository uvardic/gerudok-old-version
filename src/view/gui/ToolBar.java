package view.gui;

import javax.swing.JToolBar;

import controller.ActionManager;
import core.MainFrame;

public class ToolBar extends JToolBar {

	private static final long serialVersionUID = 1L;

	public ToolBar() {
		setFloatable(false);

		ActionManager actions = MainFrame.getInstance().getActionManager();

		add(actions.getNewTreeNode());
		add(actions.getDefaultSlotNode());
		add(actions.getSaveProjectAction());
		addSeparator();
		add(actions.getUndoAction());
		add(actions.getRedoAction());
		add(actions.getCutAction());
		add(actions.getCopyAction());
		add(actions.getPasteAction());
		add(actions.getDeleteElement());
		addSeparator();
		add(actions.getZoomIn());
		add(actions.getZoomOut());
		add(actions.getZoomToFit());
	}
}
