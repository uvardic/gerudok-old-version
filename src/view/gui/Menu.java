package view.gui;

import javax.swing.JMenu;
import javax.swing.JMenuBar;

import controller.ActionManager;
import core.MainFrame;

public class Menu extends JMenuBar {

	private static final long serialVersionUID = 1L;

	public Menu() {
		ActionManager actions = MainFrame.getInstance().getActionManager();

		JMenu file = new JMenu("File");
		JMenu view = new JMenu("View");
		JMenu edit = new JMenu("Edit");
		JMenu about = new JMenu("About");

		file.add(actions.getNewTreeNode());
		file.add(actions.getDefaultSlotNode());
		file.addSeparator();
		file.add(actions.getSaveProjectAction());
		file.add(actions.getOpenProjectAction());

		view.add(actions.getCascade());
		view.add(actions.getTileH());
		view.add(actions.getTileV());
		view.addSeparator();
		view.add(actions.getZoomIn());
		view.add(actions.getZoomOut());
		view.add(actions.getZoomToFit());

		edit.add(actions.getUndoAction());
		edit.add(actions.getRedoAction());
		edit.addSeparator();
		edit.add(actions.getCutAction());
		edit.add(actions.getCopyAction());
		edit.add(actions.getPasteAction());
		edit.addSeparator();
		edit.add(actions.getSelectAllAction());
		edit.add(actions.getDeleteElement());

		add(file);
		add(view);
		add(edit);
		add(about);
	}

}
