package view.gui;

import javax.swing.ButtonGroup;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

import controller.ActionManager;
import core.MainFrame;

public class Palette extends JToolBar {

	private static final long serialVersionUID = 1L;

	private JToggleButton rectangleButton;
	private JToggleButton linkButton;
	private JToggleButton textButton;
	private JToggleButton selectButton;
	private JToggleButton circleButton;
	private JToggleButton triangleButton;

	// Implementacija palette sa desne strane GUI aplikacije
	// Gerudok koja nam omogu�ava da izaberemo koji od
	// pou�enih oblika �elimo da nacrtamo ili mo�da da
	// selektujemo.

	public Palette() {
		super("Pallete", SwingConstants.VERTICAL);
		ButtonGroup buttons = new ButtonGroup();
		ActionManager actions = MainFrame.getInstance().getActionManager();

		selectButton = new JToggleButton(actions.getSelectAction());
		selectButton.setHideActionText(true);

		linkButton = new JToggleButton(actions.getLinkAction());
		linkButton.setHideActionText(true);

		textButton = new JToggleButton(actions.getTextAction());
		textButton.setHideActionText(true);

		rectangleButton = new JToggleButton(actions.getRectangleAction());
		rectangleButton.setHideActionText(true);

		circleButton = new JToggleButton(actions.getCircleAction());
		circleButton.setHideActionText(true);

		triangleButton = new JToggleButton(actions.getTriangleAction());
		triangleButton.setHideActionText(true);

		buttons.add(selectButton);
		buttons.add(linkButton);
		buttons.add(textButton);
		buttons.add(rectangleButton);
		buttons.add(circleButton);
		buttons.add(triangleButton);

		add(selectButton);
		add(linkButton);
		add(textButton);
		add(rectangleButton);
		add(circleButton);
		add(triangleButton);
	}

	public JToggleButton getRectangleButton() {
		return rectangleButton;
	}

	public void setRectangleButton(JToggleButton rectangleButton) {
		this.rectangleButton = rectangleButton;
	}

	public JToggleButton getSelectButton() {
		return selectButton;
	}

	public void setSelectButton(JToggleButton selectButton) {
		this.selectButton = selectButton;
	}

	public JToggleButton getCircleButton() {
		return circleButton;
	}

	public void setCircleButton(JToggleButton circleButton) {
		this.circleButton = circleButton;
	}

	public JToggleButton getTriangleButton() {
		return triangleButton;
	}

	public void setTriangleButton(JToggleButton triangleButton) {
		this.triangleButton = triangleButton;
	}

	public JToggleButton getLinkButton() {
		return linkButton;
	}

	public void setLinkButton(JToggleButton linkButton) {
		this.linkButton = linkButton;
	}

}
