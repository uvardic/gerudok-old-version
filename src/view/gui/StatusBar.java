package view.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import utilities.Constants;

public class StatusBar extends JPanel {

	private static final long serialVersionUID = 1L;

	private StatusPanel state = new StatusPanel("Current State: ");
	private StatusPanel point = new StatusPanel("Cursor Point: ");
	private StatusPanel dimension = new StatusPanel("Element Dimension: ");
	private StatusPanel selectedType = new StatusPanel("Element Type: ");
	private StatusPanel selectedName = new StatusPanel("Element Name: ");

	public StatusBar() {
		setLayout(new FlowLayout(FlowLayout.LEFT));
		add(state);
		add(point);
		add(selectedName);
		add(selectedType);
		add(dimension);
	}

	private class StatusPanel extends JLabel {

		private static final long serialVersionUID = 1L;

		public StatusPanel(String text) {
			setBorder(BorderFactory.createLineBorder(Color.GRAY, 1, true));
			setPreferredSize(new Dimension(Constants.WIDTH / 5 - 6, 20));
			setHorizontalAlignment(CENTER);
			setText(text);
		}

	}

	public StatusPanel getState() {
		return state;
	}

	public void setState(String text) {
		this.state.setText(text);
	}

	public StatusPanel getPoint() {
		return point;
	}

	public void setPoint(String text) {
		this.point.setText(text);
	}

	public StatusPanel getDimension() {
		return dimension;
	}

	public void setDimension(String text) {
		this.dimension.setText(text);
	}

	public StatusPanel getSelectedType() {
		return selectedType;
	}

	public void setSelectedType(String text) {
		this.selectedType.setText(text);
	}

	public StatusPanel getSelectedName() {
		return selectedName;
	}

	public void setSelectedName(String text) {
		this.selectedName.setText(text);
	}

}
