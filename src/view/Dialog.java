package view;

import javax.swing.JOptionPane;

import core.MainFrame;

public class Dialog {

	public static void message(String message) {
		JOptionPane.showMessageDialog(MainFrame.getInstance(), message);
	}

	public static String inputDialog(String name) {
		String input = JOptionPane.showInputDialog(MainFrame.getInstance(), "Enter " + name + " name");

		return input;
	}

}
