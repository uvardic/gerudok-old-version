package tree;

import java.awt.Component;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import tree.nodes.DiagramNode;
import tree.nodes.ElementNode;
import tree.nodes.FolderNode;
import tree.nodes.PageNode;
import tree.nodes.ProjectNode;
import tree.nodes.SlotNode;
import tree.nodes.WorkspaceNode;

public class TreeCellRenderer extends DefaultTreeCellRenderer {

	private static final long serialVersionUID = 1L;

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf,
			int row, boolean hasFocus) {
		super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

		if (value instanceof WorkspaceNode) {
			String imgUrl = "/icons/treeIcons/WorkspaceIcon.png";
			URL resource = getClass().getResource(imgUrl);
			Icon icon = null;
			if (resource != null)
				icon = new ImageIcon(resource);
			setIcon(icon);
			setText(value.toString());
		}

		if (value instanceof ProjectNode) {
			String imgUrl = "/icons/treeIcons/ProjectIcon.png";
			URL resource = getClass().getResource(imgUrl);
			Icon icon = null;
			if (resource != null)
				icon = new ImageIcon(resource);
			setIcon(icon);
			setText(value.toString());
		}

		if (value instanceof DiagramNode) {
			String imgUrl = "/icons/treeIcons/DiagramIcon.png";
			URL resource = getClass().getResource(imgUrl);
			Icon icon = null;
			if (resource != null)
				icon = new ImageIcon(resource);
			setIcon(icon);
			setText(value.toString());
		}

		if (value instanceof PageNode) {
			String imgUrl = "/icons/treeIcons/PageIcon.png";
			URL resource = getClass().getResource(imgUrl);
			Icon icon = null;
			if (resource != null)
				icon = new ImageIcon(resource);
			setIcon(icon);
			setText(value.toString());
		}

		if (value instanceof SlotNode) {
			String imgUrl = "/icons/treeIcons/SlothIcon.png";
			URL resource = getClass().getResource(imgUrl);
			Icon icon = null;
			if (resource != null)
				icon = new ImageIcon(resource);
			setIcon(icon);
			setText(value.toString());
		}

		if (value instanceof FolderNode) {
			String imgUrl = "/icons/treeIcons/FolderIcon.png";
			URL resource = getClass().getResource(imgUrl);
			Icon icon = null;
			if (resource != null)
				icon = new ImageIcon(resource);
			setIcon(icon);
			setText(value.toString());
		}

		if (value instanceof ElementNode) {
			String imgUrl = "/icons/treeIcons/ElementIcon.png";
			URL resource = getClass().getResource(imgUrl);
			Icon icon = null;
			if (resource != null)
				icon = new ImageIcon(resource);
			setIcon(icon);
			setText(value.toString());
		}

		return this;
	}

}
