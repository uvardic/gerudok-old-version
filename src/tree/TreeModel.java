package tree;

import javax.swing.tree.DefaultTreeModel;

import tree.nodes.ProjectNode;
import tree.nodes.WorkspaceNode;

public class TreeModel extends DefaultTreeModel {

	private static final long serialVersionUID = 1L;

	public TreeModel() {
		super(new WorkspaceNode());
	}

	public void addProjectNode(ProjectNode project) {
		((WorkspaceNode) getRoot()).addProjectNode(project);
	}

}
