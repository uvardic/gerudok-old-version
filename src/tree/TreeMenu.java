package tree;

import javax.swing.JPopupMenu;
import javax.swing.tree.TreeNode;

import controller.ActionManager;
import core.MainFrame;
import tree.nodes.ElementNode;
import tree.nodes.SlotNode;
import tree.nodes.WorkspaceNode;

public class TreeMenu extends JPopupMenu {

	private static final long serialVersionUID = 1L;

	public TreeMenu(TreeNode node) {
		ActionManager actions = MainFrame.getInstance().getActionManager();

		// Biramo koje akcije hocemo da dodajemo za trenutnu granu
		if (node instanceof WorkspaceNode) {
			add(actions.getNewTreeNode());
		}

		else if (node instanceof ElementNode) {
			add(actions.getDeleteTreeNode());
		}
		
		else if (node instanceof SlotNode) {
			add(actions.getDeleteTreeNode());
			add(actions.getLinkSlots());
		}

		else {
			add(actions.getNewTreeNode());
			add(actions.getDeleteTreeNode());
		}

	}

}
