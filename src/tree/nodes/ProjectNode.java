package tree.nodes;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

import javax.swing.SwingUtilities;
import javax.swing.tree.TreeNode;

import core.MainFrame;
import event.UpdateEvent;
import event.UpdateListener;
import view.Diagram;

public class ProjectNode implements TreeNode, UpdateListener, Serializable {

	private static final long serialVersionUID = -711054635557589202L;

	private String name;
	private WorkspaceNode parent;

	private transient boolean changed;
	private File projectFile;

	private ArrayList<DiagramNode> diagrams = new ArrayList<>();

	public ProjectNode(String name) {
		this.name = name;
		changed = false;
		projectFile = null;
	}

	public void addDiagramNode(DiagramNode diagramNode) {
		diagramNode.setName(diagramNode.getName() + " - " + Diagram.openFrames);
		diagrams.add(diagramNode);
		diagramNode.setParent(this);
	}

	@Override
	public String toString() {
		return ((changed ? "* " : "") + name);
	}

	// ----------------------UPDATE---------------------------------------------------------------

	@Override
	public void updatePerformed(UpdateEvent e) {
		setChanged(true);
	}

	public void setChanged(boolean changed) {
		if (this.changed != changed) {
			this.changed = changed;
			SwingUtilities.updateComponentTreeUI(MainFrame.getInstance().getTree());
		}
	}

	// ----------------------GETTERS---------------------------------------------------------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<DiagramNode> getDiagrams() {
		return diagrams;
	}

	public void setDiagrams(ArrayList<DiagramNode> diagrams) {
		this.diagrams = diagrams;
	}

	public File getProjectFile() {
		return projectFile;
	}

	public void setProjectFile(File projectFile) {
		this.projectFile = projectFile;
	}

	public boolean isChanged() {
		return changed;
	}

	public void setParent(WorkspaceNode parent) {
		this.parent = parent;
	}

	// ----------------------TREENODE--------------------------------------------------------------

	@Override
	public Enumeration<DiagramNode> children() {
		return Collections.enumeration(diagrams);
	}

	@Override
	public boolean getAllowsChildren() {
		return true;
	}

	@Override
	public TreeNode getChildAt(int childIndex) {
		return diagrams.get(childIndex);
	}

	@Override
	public int getChildCount() {
		return diagrams.size();
	}

	@Override
	public int getIndex(TreeNode node) {
		return diagrams.indexOf(node);
	}

	@Override
	public TreeNode getParent() {
		return parent;
	}

	@Override
	public boolean isLeaf() {
		return false;
	}

}
