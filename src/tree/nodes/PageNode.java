package tree.nodes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

import javax.swing.tree.TreeNode;

import view.Slot;

public class PageNode implements TreeNode, Serializable {

	private static final long serialVersionUID = -1300070266071066678L;

	private String name;
	private DiagramNode parent;

	private ArrayList<SlotNode> slots = new ArrayList<>();

	public PageNode(String name) {
		this.name = name;
	}

	public void addSlotNode(SlotNode slotNode) {
		slotNode.setName(slotNode.getName() + " - " + Slot.openSlots);
		slots.add(slotNode);
		slotNode.setParent(this);
		slotNode.getSlotModel().addUpdateListener((ProjectNode) parent.getParent());
	}

	@Override
	public String toString() {
		return name;
	}

	// ----------------------GETTERS---------------------------------------------------------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<SlotNode> getSlots() {
		return slots;
	}

	public void setSlots(ArrayList<SlotNode> slots) {
		this.slots = slots;
	}

	public void setParent(DiagramNode parent) {
		this.parent = parent;
	}

	// ----------------------TREENODE--------------------------------------------------------------

	@Override
	public Enumeration<SlotNode> children() {
		return Collections.enumeration(slots);
	}

	@Override
	public boolean getAllowsChildren() {
		return true;
	}

	@Override
	public TreeNode getChildAt(int childIndex) {
		return slots.get(childIndex);
	}

	@Override
	public int getChildCount() {
		return slots.size();
	}

	@Override
	public int getIndex(TreeNode node) {
		return slots.indexOf(node);
	}

	@Override
	public TreeNode getParent() {
		return parent;
	}

	@Override
	public boolean isLeaf() {
		return false;
	}

}
