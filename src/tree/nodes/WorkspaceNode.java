package tree.nodes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

import javax.swing.tree.TreeNode;

public class WorkspaceNode implements TreeNode, Serializable {

	private static final long serialVersionUID = 239311806595167243L;

	private ArrayList<ProjectNode> projects = new ArrayList<>();

	public WorkspaceNode() {
		super();
	}

	public void addProjectNode(ProjectNode projectNode) {
		projects.add(projectNode);
		if (!projectNode.getName().contains(" - " + projects.size())) {
			projectNode.setName(projectNode.getName() + " - " + projects.size());
		}
		projectNode.setParent(this);
	}

	@Override
	public String toString() {
		return "Workspace";
	}

	// ----------------------GETTERS---------------------------------------------------------------

	public ArrayList<ProjectNode> getProjects() {
		return projects;
	}

	public void setProjects(ArrayList<ProjectNode> projects) {
		this.projects = projects;
	}

	// ----------------------TREENODE--------------------------------------------------------------

	@Override
	public Enumeration<ProjectNode> children() {
		return Collections.enumeration(projects);
	}

	@Override
	public boolean getAllowsChildren() {
		return true;
	}

	@Override
	public TreeNode getChildAt(int childIndex) {
		return projects.get(childIndex);
	}

	@Override
	public int getChildCount() {
		return projects.size();
	}

	@Override
	public int getIndex(TreeNode node) {
		return projects.indexOf(node);
	}

	@Override
	public TreeNode getParent() {
		return null;
	}

	@Override
	public boolean isLeaf() {
		return false;
	}

}
