package tree.nodes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

import javax.swing.tree.TreeNode;

public class FolderNode implements TreeNode, Serializable {

	private static final long serialVersionUID = -8823937334816670178L;

	private SlotNode parent;
	private String name;

	private ArrayList<ElementNode> elements = new ArrayList<>();

	public FolderNode(String name, SlotNode parent) {
		this.name = name;
		this.parent = parent;
	}

	public void addElementNode(ElementNode elementNode) {
		elements.add(elementNode);
	}

	public void removeElementNode(ElementNode elementNode) {
		elements.remove(elementNode);
	}

	public void removeLastElementNode() {
		elements.remove(elements.size() - 1);
	}

	@Override
	public String toString() {
		return name;
	}

	// ----------------------GETTERS---------------------------------------------------------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<ElementNode> getElements() {
		return elements;
	}

	public void setElements(ArrayList<ElementNode> elements) {
		this.elements = elements;
	}

	public ElementNode getLastElementNode() {
		return elements.get(elements.size() - 1);
	}

	public void setParent(SlotNode parent) {
		this.parent = parent;
	}

	// ----------------------TREENODE--------------------------------------------------------------

	@Override
	public Enumeration<ElementNode> children() {
		return Collections.enumeration(elements);
	}

	@Override
	public boolean getAllowsChildren() {
		return true;
	}

	@Override
	public TreeNode getChildAt(int childIndex) {
		return elements.get(childIndex);
	}

	@Override
	public int getChildCount() {
		return elements.size();
	}

	@Override
	public int getIndex(TreeNode node) {
		return elements.indexOf(node);
	}

	@Override
	public TreeNode getParent() {
		return parent;
	}

	@Override
	public boolean isLeaf() {
		return false;
	}

}
