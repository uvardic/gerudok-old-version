package tree.nodes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

import javax.swing.tree.TreeNode;

import model.SlotModel;
import model.SlotSelectionModel;

public class SlotNode implements TreeNode, Serializable {

	private static final long serialVersionUID = -1326873803704940563L;

	private String name;
	private PageNode parent;

	private SlotModel slotModel = new SlotModel();
	private SlotSelectionModel selectionModel = null;

	private ArrayList<FolderNode> folders = new ArrayList<>();

	public SlotNode(String name) {
		this.name = name;
		folders.add(new FolderNode("Devices", this));
		folders.add(new FolderNode("Links", this));
	}

	@Override
	public String toString() {
		return name;
	}

	public SlotSelectionModel getSelectionModel() {
		if (selectionModel == null) {
			selectionModel = new SlotSelectionModel();
		}

		return selectionModel;
	}

	// ----------------------GETTERS---------------------------------------------------------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SlotModel getSlotModel() {
		return slotModel;
	}

	public void setSlotModel(SlotModel slotModel) {
		this.slotModel = slotModel;
	}

	public ArrayList<FolderNode> getFolders() {
		return folders;
	}

	public void setFolders(ArrayList<FolderNode> folders) {
		this.folders = folders;
	}

	public void setSelectionModel(SlotSelectionModel selectionModel) {
		this.selectionModel = selectionModel;
	}

	public void setParent(PageNode parent) {
		this.parent = parent;
	}

	// ----------------------TREENODE--------------------------------------------------------------

	@Override
	public Enumeration<FolderNode> children() {
		return Collections.enumeration(folders);
	}

	@Override
	public boolean getAllowsChildren() {
		return true;
	}

	@Override
	public TreeNode getChildAt(int childIndex) {
		return folders.get(childIndex);
	}

	@Override
	public int getChildCount() {
		return folders.size();
	}

	@Override
	public int getIndex(TreeNode node) {
		return folders.indexOf(node);
	}

	@Override
	public TreeNode getParent() {
		return parent;
	}

	@Override
	public boolean isLeaf() {
		return false;
	}

}
