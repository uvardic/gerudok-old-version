package tree.nodes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

import javax.swing.tree.TreeNode;

import view.Page;

public class DiagramNode implements TreeNode, Serializable {

	private static final long serialVersionUID = -2493444475580404472L;

	private String name;
	private ProjectNode parent;

	private ArrayList<PageNode> pages = new ArrayList<>();

	public DiagramNode(String name) {
		this.name = name;
	}

	public void addPageNode(PageNode pageNode) {
		pageNode.setName(pageNode.getName() + " - " + Page.openPages);
		pages.add(pageNode);
		pageNode.setParent(this);
	}

	@Override
	public String toString() {
		return name;
	}

	// ----------------------GETTERS---------------------------------------------------------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<PageNode> getPages() {
		return pages;
	}

	public void setPages(ArrayList<PageNode> pages) {
		this.pages = pages;
	}

	public void setParent(ProjectNode parent) {
		this.parent = parent;
	}

	// ----------------------TREENODE--------------------------------------------------------------

	@Override
	public Enumeration<PageNode> children() {
		return Collections.enumeration(pages);
	}

	@Override
	public boolean getAllowsChildren() {
		return true;
	}

	@Override
	public TreeNode getChildAt(int childIndex) {
		return pages.get(childIndex);
	}

	@Override
	public int getChildCount() {
		return pages.size();
	}

	@Override
	public int getIndex(TreeNode node) {
		return pages.indexOf(node);
	}

	@Override
	public TreeNode getParent() {
		return parent;
	}

	@Override
	public boolean isLeaf() {
		return false;
	}

}
