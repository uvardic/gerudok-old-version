package tree.nodes;

import java.io.Serializable;
import java.util.Enumeration;

import javax.swing.tree.TreeNode;

import core.MainFrame;
import model.elements.SlotElement;
import model.elements.deviceElements.SlotDevice;
import view.Diagram;
import view.Page;
import view.Slot;

public class ElementNode implements TreeNode, Serializable {

	private static final long serialVersionUID = -608923166872167457L;

	private String name;
	private FolderNode parent;

	public ElementNode(SlotElement element) {
		name = element.getName();

		Diagram diagram = MainFrame.getInstance().getSelectedDiagram();
		Page page = diagram.getSelectedPage();
		Slot slot = page.getSelectedSlot();

		if (element instanceof SlotDevice) {
			parent = slot.getSlotNode().getFolders().get(0);
		} else {
			parent = slot.getSlotNode().getFolders().get(1);
		}

		parent.addElementNode(this);
	}

	@Override
	public String toString() {
		return name;
	}

	// ----------------------GETTERS---------------------------------------------------------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// ----------------------TREENODE--------------------------------------------------------------

	@Override
	public Enumeration<ElementNode> children() {
		return null;
	}

	@Override
	public boolean getAllowsChildren() {
		return false;
	}

	@Override
	public TreeNode getChildAt(int childIndex) {
		return null;
	}

	@Override
	public int getChildCount() {
		return 0;
	}

	@Override
	public int getIndex(TreeNode node) {
		return 0;
	}

	@Override
	public TreeNode getParent() {
		return parent;
	}

	@Override
	public boolean isLeaf() {
		return true;
	}

}
