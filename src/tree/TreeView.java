package tree;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyVetoException;
import java.util.ArrayList;

import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import core.MainFrame;
import model.elements.SlotElement;
import tree.nodes.DiagramNode;
import tree.nodes.ElementNode;
import tree.nodes.PageNode;
import tree.nodes.ProjectNode;
import tree.nodes.SlotNode;
import tree.nodes.WorkspaceNode;
import view.Diagram;
import view.Page;
import view.Slot;

public class TreeView extends JTree implements TreeSelectionListener, MouseListener {

	private static final long serialVersionUID = 1L;

	public TreeView() {
		addMouseListener(this);
		addTreeSelectionListener(this);
		setCellRenderer(new TreeCellRenderer());
		setCellEditor(new TreeCellEditor(this, new DefaultTreeCellRenderer()));
	}

	// Direktno dodavanje projekta na drvo
	public void addProjectNode(ProjectNode project) {
		((WorkspaceNode) getModel().getRoot()).addProjectNode(project);
	}

	public ProjectNode getCurrentProjectNode() {
		TreePath path = getSelectionPath();
		if (path != null) {
			for (int i = 0; i < path.getPathCount(); i++) {
				if (path.getPathComponent(i) instanceof ProjectNode) {
					return (ProjectNode) path.getPathComponent(i);
				}
			}
		}

		return null;
	}

	@Override
	public void valueChanged(TreeSelectionEvent e) {
		// Poslednja selektovana grana
		Object o = e.getPath().getLastPathComponent();
		ArrayList<Diagram> diagrams = MainFrame.getInstance().getDiagrams();

		if (o instanceof DiagramNode) {
			DiagramNode diagramNode = (DiagramNode) o;
			for (Diagram diagram : diagrams) {
				if (diagram.getName().equals(diagramNode.getName())) {
					try {
						// Metoda show se poziva da bi otvorili neki od "zatvorenih" diagrama
						// Diagramov default close operation je HIDE_ON_CLOSE
						diagram.show();
						diagram.setSelected(true);
					} catch (PropertyVetoException e1) {
						e1.printStackTrace();
					}
				}
			}
		}

		if (o instanceof PageNode) {
			PageNode pageNode = (PageNode) o;
			for (Diagram diagram : diagrams) {
				for (Page page : diagram.getPages()) {
					if (page.getName().equals(pageNode.getName())) {
						try {
							diagram.show();
							diagram.setSelected(true);
							diagram.setSelectedPage(page);
						} catch (PropertyVetoException e1) {
							e1.printStackTrace();
						}

					}
				}
			}
		}

		if (o instanceof SlotNode) {
			SlotNode slotNode = (SlotNode) o;
			for (Diagram diagram : diagrams) {
				for (Page page : diagram.getPages()) {
					for (Slot slot : page.getSlots()) {
						if (slot.getName().equals(slotNode.getName())) {
							try {
								diagram.show();
								diagram.setSelected(true);
								diagram.setSelectedPage(page);
								page.setSelectedSlot(slot);
							} catch (PropertyVetoException e1) {
								e1.printStackTrace();
							}
						}
					}
				}
			}
		}

		if (o instanceof ElementNode) {
			ElementNode elementNode = (ElementNode) o;
			for (Diagram diagram : diagrams) {
				for (Page page : diagram.getPages()) {
					for (Slot slot : page.getSlots()) {
						for (SlotElement element : slot.getSlotNode().getSlotModel().getElements()) {
							if (element.getName().equals(elementNode.getName())) {
								try {
									diagram.show();
									diagram.setSelected(true);
									diagram.setSelectedPage(page);
									page.setSelectedSlot(slot);
									slot.getSlotNode().getSelectionModel().removeAllFromSelectionList();
									slot.getSlotNode().getSelectionModel().addToSelectionList(element);
								} catch (PropertyVetoException e1) {
									e1.printStackTrace();
								}
							}
						}
					}
				}
			}
		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON3) {
			TreePath path = getClosestPathForLocation(e.getX(), e.getY());
			setSelectionPath(path);
			TreeNode node = (TreeNode) getLastSelectedPathComponent();
			TreeMenu treeMenu = new TreeMenu(node);
			treeMenu.show(e.getComponent(), e.getX(), e.getY());
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

}
