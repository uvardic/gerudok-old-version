package state;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.Iterator;

import commands.MoveDeviceCommand;
import model.elements.SlotElement;
import model.elements.deviceElements.SlotDevice;
import view.Slot;

public class MoveState extends State {

	private Slot slot;
	private double deltaX = 0;
	private double deltaY = 0;

	public MoveState(Slot slot) {
		this.slot = slot;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		slot.getFramework().setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
		Point2D lastPoint = e.getPoint();
		slot.transformToUserSpace(lastPoint);
		double lastX = lastPoint.getX() - slot.getLastPoint().getX();
		double lastY = lastPoint.getY() - slot.getLastPoint().getY();

		Iterator<SlotElement> it = slot.getSlotNode().getSelectionModel().getSelectionList().iterator();
		while (it.hasNext()) {
			SlotElement element = it.next();
			if (element instanceof SlotDevice) {
				SlotDevice device = (SlotDevice) element;
				Point2D newPoint = device.getPoint();
				newPoint.setLocation(newPoint.getX() + lastX, newPoint.getY() + lastY);
				device.setPoint(newPoint);
			}
		}
		deltaX += lastX;
		deltaY += lastY;
		slot.setLastPoint(lastPoint);
		slot.repaint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		slot.getCommandManager()
				.addCommand(new MoveDeviceCommand(slot.getSlotNode().getSelectionModel(), deltaX, deltaY));
		slot.getFramework().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		deltaX = 0;
		deltaY = 0;
		Point2D lastPoint = e.getPoint();
		slot.transformToUserSpace(lastPoint);
		slot.setLastPoint(lastPoint);
		slot.startSelectionState();
	}

}
