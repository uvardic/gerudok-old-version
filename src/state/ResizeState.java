package state;

import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.Iterator;

import model.elements.SlotElement;
import model.elements.deviceElements.SlotDevice;
import view.Handle;
import view.Slot;

public class ResizeState extends State {

	private Slot slot;
	private Handle handleInMotion = null;

	public ResizeState(Slot slot) {
		this.slot = slot;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		Point2D point = e.getPoint();
		slot.transformToUserSpace(point);

		if (handleInMotion == null) {
			handleInMotion = slot.getDeviceAndHandleForPoint(point);
		}

		if (handleInMotion != null) {
			Iterator<SlotElement> it = slot.getSlotNode().getSelectionModel().getSelectionList().iterator();
			while (it.hasNext()) {
				SlotElement element = it.next();
				if (element instanceof SlotDevice) {
					SlotDevice device = (SlotDevice) element;

					switch (handleInMotion.ordinal()) {
					case 6: {
						double newWidth = device.getWidth() + point.getX() - (device.getPointX() + device.getWidth());
						double newHeight = device.getHeight() + point.getY()
								- (device.getPointY() + device.getHeight());
						double scaleX = newWidth / device.getInitialSize().getWidth();
						double scaleY = newHeight / device.getInitialSize().getHeight();
						double newScale = 1;

						if (scaleX < scaleY) {
							newScale = scaleX;
						} else {
							newScale = scaleY;
						}

						if (newScale < 0.2) {
							device.setScale(0.2);
						} else if (newScale > 3) {
							device.setScale(3);
						} else {
							device.setScale(newScale);
						}

						break;
					}
					}
				}
				slot.repaint();
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		handleInMotion = null;
		slot.startSelectionState();
	}

}
