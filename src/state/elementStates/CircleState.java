package state.elementStates;

import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import commands.AddDeviceCommand;
import core.MainFrame;
import model.SlotModel;
import model.SlotSelectionModel;
import model.elements.ElementType;
import state.State;
import view.Diagram;
import view.Page;
import view.Slot;

public class CircleState extends State {

	private Slot slot;

	public CircleState(Slot slot) {
		this.slot = slot;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		Point2D point = e.getPoint();
		slot.transformToUserSpace(point);

		if (e.getButton() == MouseEvent.BUTTON1) {
			if (slot.getSlotNode().getSlotModel().getElementAtPosition(point) == -1) {
				Diagram diagram = MainFrame.getInstance().getSelectedDiagram();
				Page page = diagram.getSelectedPage();
				Slot slot = page.getSelectedSlot();
				SlotModel slotModel = slot.getSlotNode().getSlotModel();
				SlotSelectionModel slotSelectionModel = slot.getSlotNode().getSelectionModel();
				slot.getCommandManager()
						.addCommand(new AddDeviceCommand(slotModel, slotSelectionModel, point, ElementType.CIRCLE));
			}
		}

	}

}