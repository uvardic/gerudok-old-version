package state.elementStates;

import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import javax.swing.SwingUtilities;

import commands.AddLinkCommand;
import core.MainFrame;
import model.elements.InputOutputElement;
import model.elements.deviceElements.SlotDevice;
import model.elements.deviceElements.TextElement;
import model.elements.linkElements.LinkElement;
import state.State;
import tree.nodes.ElementNode;
import view.Slot;

public class LinkState extends State {

	private Slot slot;
	private LinkElement link;

	public LinkState(Slot slot) {
		this.slot = slot;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		Point2D point = e.getPoint();
		slot.transformToUserSpace(point);

		if (e.getButton() == MouseEvent.BUTTON1) {
			int devicePoint = slot.getSlotNode().getSlotModel().getElementAtPosition(slot.getLastPoint());

			// Ako nije u toku iscrtavanje i ako je na poziciji element kreiramo link
			if (devicePoint != -1 && link == null) {
				SlotDevice startDevice = (SlotDevice) slot.getSlotNode().getSlotModel().getElements().get(devicePoint);
				if (!(startDevice instanceof TextElement)) {
					link = (LinkElement) LinkElement.createDefault(startDevice, startDevice.getFirstOutput());
					slot.getSlotNode().getSlotModel().addElement(link);
					// Referenca na link, koristi se kod brisanja
					startDevice.getFirstOutput().addLink(link);
				}
			}

			// Ako je u toku isctavanje i na poziciji nema elementa dodaj prekidnu tacku
			else if (devicePoint == -1 && link != null) {
				link.addPoint(point);
			}

			// Ako je u toku iscrtavanje i na poziciji postoji element zavrsi iscrtavanje
			else if (devicePoint != -1 && link != null) {
				SlotDevice endDevice = (SlotDevice) slot.getSlotNode().getSlotModel().getElements().get(devicePoint);
				if (!(endDevice instanceof TextElement)) {
					InputOutputElement closestInput = endDevice.getClosestInput(point);

					closestInput.addLink(link);
					new ElementNode(link);
					SwingUtilities.updateComponentTreeUI(MainFrame.getInstance().getTree());
					slot.getCommandManager().addCommand(new AddLinkCommand(slot.getSlotNode().getSlotModel(), link));
					link.setInput(closestInput);
					link.setEndDevice(endDevice);
					link = null;
				}
			}

		}

		else if (e.getButton() == MouseEvent.BUTTON3) {
			// Ako je u toku iscrtavanje linka prekidamo iscrtavanje i brisemo linkove
			if (link != null) {
				for (int i = link.getPoints().size() - 1; i >= 0; i--) {
					link.getPoints().remove(i);
				}
				link = null;
			}
		}

		slot.repaint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		Point2D point = e.getPoint();
		slot.transformToUserSpace(point);

		// Provera da li je iscrtavanje u toku
		if (link == null) {
			return;
		}

		if (e.getButton() == MouseEvent.BUTTON1) {
			int devicePoint = slot.getSlotNode().getSlotModel().getElementAtPosition(point);

			// Mis se nalazi na nekom devicu
			if (devicePoint != -1) {
				SlotDevice endDevice = (SlotDevice) slot.getSlotNode().getSlotModel().getElements().get(devicePoint);
				if (!(endDevice instanceof TextElement)) {
					InputOutputElement closestInput = endDevice.getClosestInput(point);

					closestInput.addLink(link);
					new ElementNode(link);
					SwingUtilities.updateComponentTreeUI(MainFrame.getInstance().getTree());
					slot.getCommandManager().addCommand(new AddLinkCommand(slot.getSlotNode().getSlotModel(), link));
					link.setInput(closestInput);
					link.setEndDevice(endDevice);
					link = null;
				}
			} else {
				link.addPoint(point);
			}
		}

		slot.repaint();
	}

	// Povlacenje linka, azuriramo lokaciju poslednje tacke
	@Override
	public void mouseDragged(MouseEvent e) {
		if (link != null) {
			link.getLastPoint().setLocation(e.getPoint());
			slot.transformToUserSpace(link.getLastPoint());
		}
		slot.repaint();
	}

	// Pomeranje misa, azuriramo lokaciju poslednje tacke
	@Override
	public void mouseMoved(MouseEvent e) {
		if (link != null) {
			link.getLastPoint().setLocation(e.getPoint());
			slot.transformToUserSpace(link.getLastPoint());
		}
		slot.repaint();
	}

}
