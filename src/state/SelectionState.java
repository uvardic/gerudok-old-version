package state;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import core.MainFrame;
import model.elements.SlotElement;
import model.elements.deviceElements.SlotDevice;
import model.elements.deviceElements.TextElement;
import view.Handle;
import view.Slot;
import view.gui.Dialog;

public class SelectionState extends State {

	private Slot slot;

	// Hendl na poziciji
	private Handle handleInMotion = null;
	// Indeks selektovanog elementa
	private int elementInMotion = -1;

	// Kliknuto dugme misa
	private int mouseButton = 0;

	public SelectionState(Slot slot) {
		this.slot = slot;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		mouseButton = e.getButton();
		Point2D point = e.getPoint();
		slot.transformToUserSpace(point);

		if (e.getButton() == MouseEvent.BUTTON1) {
			handleInMotion = slot.getDeviceAndHandleForPoint(point);

			if (handleInMotion == null) {
				elementInMotion = slot.getSlotNode().getSlotModel().getElementAtPosition(point);

				// Ako ne drzimo Control izbacujemo sve elemente iz liste selektovanih
				if (!e.isControlDown()) {
					slot.getSlotNode().getSelectionModel().removeAllFromSelectionList();
				}

				// Ako smo selektovali elemet
				if (elementInMotion != -1) {
					SlotElement element = slot.getSlotNode().getSlotModel().getElements().get(elementInMotion);

					// Ako je vec selektovan brisemo ga iz liste
					if (slot.getSlotNode().getSelectionModel().getSelectionList().contains(element)) {
						slot.getSlotNode().getSelectionModel().removeFromSelectionList(element);
					} else {
						slot.getSlotNode().getSelectionModel().addToSelectionList(element);
					}

					// Statusna linija
					if (!slot.getSlotNode().getSelectionModel().getSelectionList().isEmpty()) {
						SlotElement lastSelectedElement = slot.getSlotNode().getSelectionModel().getLastElement();
						MainFrame.getInstance().getStatusBar()
								.setSelectedName("Element Name: " + lastSelectedElement.getName());
						MainFrame.getInstance().getStatusBar()
								.setSelectedType("Element Type: " + lastSelectedElement.getElementType());
						if (lastSelectedElement instanceof SlotDevice) {
							SlotDevice lastSelectedDevice = (SlotDevice) lastSelectedElement;
							MainFrame.getInstance().getStatusBar().setDimension("Element Dimension: width = "
									+ lastSelectedDevice.getWidth() + " height = " + lastSelectedDevice.getHeight());
						}

					}

					if (element instanceof TextElement) {
						TextElement textElement = (TextElement) element;
						if (e.getClickCount() == 2) {
							String text = Dialog.inputDialog();
							textElement.setText(text);
							if (text != null) {
								textElement.setSize(new Dimension(text.length() * 10, 30));
							}
						}
					}

					// Nije selektovan element, izbacujemo sve elemente iz liste selektovanih
				} else {
					slot.getSlotNode().getSelectionModel().removeAllFromSelectionList();
				}
			}
			slot.repaint();
		}
	}

	// Podesavamo mis u zavisnosti od pozicije
	@Override
	public void mouseMoved(MouseEvent e) {
		Point2D point = e.getPoint();
		slot.transformToUserSpace(point);
		slot.setMouseCursor(point);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (mouseButton == MouseEvent.BUTTON1) {
			Point2D point = e.getPoint();
			slot.transformToUserSpace(point);
			// Pokusavamo da selektujemo element na poslednjoj upamcenoj poziciji
			elementInMotion = slot.getSlotNode().getSlotModel().getElementAtPosition(slot.getLastPoint());

			// Selektovali smo element
			if (elementInMotion != -1) {
				SlotElement element = slot.getSlotNode().getSlotModel().getElements().get(elementInMotion);

				if (!slot.getSlotNode().getSelectionModel().getSelectionList().contains(element)) {
					if (!e.isControlDown()) {
						slot.getSlotNode().getSelectionModel().removeAllFromSelectionList();
					}
					slot.getSlotNode().getSelectionModel().addToSelectionList(element);
				}
			}

			handleInMotion = slot.getDeviceAndHandleForPoint(point);
			if (handleInMotion != null) {
				slot.startResizeState();
			} else {
				elementInMotion = slot.getSlotNode().getSlotModel().getElementAtPosition(point);
				if (elementInMotion != -1) {
					slot.startMoveState();

					return;
				} else {
					slot.startLassoState();
				}
			}
		}
	}

}
