package state;

import state.elementStates.CircleState;
import state.elementStates.LinkState;
import state.elementStates.RectangleState;
import state.elementStates.TextState;
import state.elementStates.TriangleState;
import view.Slot;

public class StateManager {
	private State currentState;

	private SelectionState selectionState;
	private LassoState lassoState;
	private MoveState moveState;
	private ResizeState resizeState;

	private LinkState linkState;

	private CircleState circleState;
	private RectangleState rectangleState;
	private TriangleState triangleState;
	private TextState textState;

	public StateManager(Slot slot) {
		selectionState = new SelectionState(slot);
		lassoState = new LassoState(slot);
		moveState = new MoveState(slot);
		resizeState = new ResizeState(slot);

		linkState = new LinkState(slot);

		circleState = new CircleState(slot);
		rectangleState = new RectangleState(slot);
		triangleState = new TriangleState(slot);
		textState = new TextState(slot);

		currentState = selectionState;
	}

	public State getCurrentState() {
		return currentState;
	}

	public void setCircleState() {
		currentState = circleState;
	}

	public void setRectangleState() {
		currentState = rectangleState;
	}

	public void setTriangleState() {
		currentState = triangleState;
	}

	public void setSelectionState() {
		currentState = selectionState;
	}

	public void setLassoState() {
		currentState = lassoState;
	}

	public void setMoveState() {
		currentState = moveState;
	}

	public void setResizeState() {
		currentState = resizeState;
	}

	public void setLinkState() {
		currentState = linkState;
	}

	public void setTextState() {
		currentState = textState;
	}
}
