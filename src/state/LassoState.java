package state;

import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import view.Slot;

public class LassoState extends State {

	private Rectangle2D rectangle = new Rectangle2D.Double();
	private Slot slot;

	public LassoState(Slot slot) {
		this.slot = slot;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		Point2D point = e.getPoint();
		slot.transformToUserSpace(point);
		double width = point.getX() - slot.getLastPoint().getX();
		double height = point.getY() - slot.getLastPoint().getY();

		if (width < 0 && height < 0) {
			rectangle.setRect(point.getX(), point.getY(), Math.abs(width), Math.abs(height));
		} else if (width < 0 && height >= 0) {
			rectangle.setRect(point.getX(), slot.getLastPoint().getY(), Math.abs(width), Math.abs(height));
		} else if (width > 0 && height < 0) {
			rectangle.setRect(slot.getLastPoint().getX(), point.getY(), Math.abs(width), Math.abs(height));
		} else {
			rectangle.setRect(slot.getLastPoint().getX(), slot.getLastPoint().getY(), Math.abs(width),
					Math.abs(height));
		}

		slot.getSlotNode().getSelectionModel().selectElement(rectangle,
				slot.getSlotNode().getSlotModel().getElements());
		slot.setSelectionRectangle(rectangle);
		slot.repaint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		slot.setSelectionRectangle(new Rectangle2D.Double(0, 0, 0, 0));
		slot.repaint();
		slot.startSelectionState();
	}

}
