package model.elements;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Stroke;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import model.elements.deviceElements.SlotDevice;
import model.elements.linkElements.LinkElement;
import painters.InputOutputPainter;
import utilities.Constants;

// Model za iscrtavanje ulaza i izlaza svakog elementa
public class InputOutputElement extends SlotElement {

	private static final long serialVersionUID = -7101212996802836639L;

	protected SlotDevice device;
	protected Point2D point;
	protected int index;
	protected ElementType type;

	protected ArrayList<LinkElement> links = new ArrayList<>();

	public InputOutputElement(Paint paint, Stroke stroke, Color strokeColor, SlotDevice device, Point2D point,
			int index, ElementType type) {
		super(paint, stroke, strokeColor);
		this.device = device;
		this.point = point;
		this.index = index;
		this.type = type;
		elementPainter = new InputOutputPainter(this);
	}

	public InputOutputElement(InputOutputElement element, SlotDevice device) {
		super(element);
		this.device = device;
		this.point = element.point;
		this.type = element.type;
		this.index = element.index;
		// elementPainter = new InputOutputPainter(this);
	}

	public static SlotElement createDefault(Point2D point, SlotDevice device, int index, ElementType type) {
		BasicStroke stroke = new BasicStroke((2), BasicStroke.CAP_SQUARE, BasicStroke.JOIN_BEVEL);
		InputOutputElement element = new InputOutputElement(Constants.DEVICE_COLOR, stroke, Color.BLACK, device, point,
				index, type);

		return element;
	}

	@Override
	public SlotElement clone() {
		return new InputOutputElement(this, device);
	}

	// ----------------------GETTERS---------------------------------------------------------------

	public LinkElement getLinkAt(int index) {
		return links.get(index);
	}

	public void removeLink(LinkElement link) {
		links.remove(link);
	}

	public void addLink(LinkElement link) {
		links.add(link);
	}

	public LinkElement getFirstLink() {
		return links.get(0);
	}

	public Point2D getPoint() {
		int absoluteX = 0;
		int absoluteY = 0;

		if (type == ElementType.INPUT) {
			absoluteX = (int) device.getPointX();
			absoluteY = (int) (device.getPointY() + (device.getHeight() / (device.getNumberOfInputs() + 1)) * (index));
		}

		else if (type == ElementType.OUTPUT) {
			absoluteX = (int) (device.getPointX() + device.getWidth());
			absoluteY = (int) (device.getPointY() + (device.getHeight() / (device.getNumberOfOutputs() + 1) * (index)));
		}

		return new Point(absoluteX, absoluteY);
	}

	public double getPointX() {
		return getPoint().getX();
	}

	public double getPointY() {
		return getPoint().getY();
	}

	public SlotDevice getDevice() {
		return device;
	}

	public void setDevice(SlotDevice device) {
		this.device = device;
	}

	public void setPoint(Point2D point) {
		this.point = point;
	}

	public ElementType getType() {
		return type;
	}

	public void setType(ElementType type) {
		this.type = type;
	}

	public ArrayList<LinkElement> getLinks() {
		return links;
	}

	public void setLinks(ArrayList<LinkElement> links) {
		this.links = links;
	}

}
