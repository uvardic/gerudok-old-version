package model.elements;

public enum ElementType {
	INPUT, OUTPUT, LINK, CIRCLE, RECTANGLE, TRIANGLE, TEXT;
}
