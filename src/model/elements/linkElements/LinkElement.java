package model.elements.linkElements;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Paint;
import java.awt.Stroke;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import model.elements.InputOutputElement;
import model.elements.SlotElement;
import model.elements.deviceElements.SlotDevice;
import painters.linkPainters.LinkPainter;

public class LinkElement extends SlotElement {

	private static final long serialVersionUID = -5801751919210124374L;

	private static int index = 0;

	// Pocetni device od kog krece crtanje veze
	protected SlotDevice startDevice;
	// Izlaz za vezu
	protected InputOutputElement output;

	// Krajni device gde se zavrsava veza
	protected SlotDevice endDevice;
	// Ulaz za vezu
	protected InputOutputElement input;

	// Lista pozicija, sluzi za iscrtavanje prekidnih tacaka
	protected ArrayList<Point2D> points = new ArrayList<>();

	public LinkElement(Paint paint, Stroke stroke, Color strokeColor, SlotDevice startDevice,
			InputOutputElement output) {
		super(paint, stroke, strokeColor);
		this.startDevice = startDevice;
		this.output = output;
		addPoint(new Point2D.Double(output.getPointX(), output.getPointY()));
		elementPainter = new LinkPainter(this);
	}

	public static SlotElement createDefault(SlotDevice startDevice, InputOutputElement output) {
		Paint paint = Color.BLACK;
		BasicStroke stroke = new BasicStroke(2, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_BEVEL);
		LinkElement link = new LinkElement(paint, stroke, Color.BLUE, startDevice, output);
		link.setName("Link - " + (++index));

		return link;
	}

	@Override
	public SlotElement clone() {
		return null;
	}

	public void addPoint(Point2D point) {
		points.add(point);
	}

	public void setEndDevice(SlotDevice endDevice) {
		this.endDevice = endDevice;
		points.remove(points.size() - 1);
	}

	public Point2D getLastPoint() {
		return points.get(points.size() - 1);
	}

	public SlotDevice getStartDevice() {
		return startDevice;
	}

	public void setStartDevice(SlotDevice startDevice) {
		this.startDevice = startDevice;
	}

	public InputOutputElement getOutput() {
		return output;
	}

	public void setOutput(InputOutputElement output) {
		this.output = output;
	}

	public InputOutputElement getInput() {
		return input;
	}

	public void setInput(InputOutputElement input) {
		this.input = input;
	}

	public ArrayList<Point2D> getPoints() {
		return points;
	}

	public void setPoints(ArrayList<Point2D> points) {
		this.points = points;
	}

	public SlotDevice getEndDevice() {
		return endDevice;
	}

	public static int getIndex() {
		return index;
	}

	public static void setIndex(int index) {
		LinkElement.index = index;
	}

	public static void plusIndex() {
		LinkElement.index++;
	}

	public static void minusIndex() {
		LinkElement.index--;
	}

}
