package model.elements;

import java.awt.Color;
import java.awt.Paint;
import java.awt.Stroke;
import java.io.Serializable;

import painters.ElementPainter;
import serialization.SerializableStroke;

public abstract class SlotElement implements Serializable {

	private static final long serialVersionUID = -3874629228508920163L;

	protected Paint paint;
	protected SerializableStroke stroke;
	protected Color strokeColor;

	protected String name;

	protected ElementPainter elementPainter;
	protected ElementType elementType;

	public SlotElement(Paint paint, Stroke stroke, Color strokeColor) {
		this.paint = paint;
		setStroke(stroke);
		this.strokeColor = strokeColor;
	}

	// Konstrkutor za kloniranje
	public SlotElement(SlotElement element) {
		this.paint = element.paint;
		this.stroke = element.stroke;
		this.strokeColor = element.strokeColor;
		this.name = element.name;
		this.elementType = element.elementType;
		this.elementPainter = element.elementPainter;
	}

	@Override
	public abstract SlotElement clone();

	public Paint getPaint() {
		return paint;
	}

	public void setPaint(Paint paint) {
		this.paint = paint;
	}

	public Stroke getStroke() {
		return stroke;
	}

	public void setStroke(Stroke stroke) {
		this.stroke = new SerializableStroke(stroke);
	}

	public Color getStrokeColor() {
		return strokeColor;
	}

	public void setStrokeColor(Color strokeColor) {
		this.strokeColor = strokeColor;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ElementPainter getElementPainter() {
		return elementPainter;
	}

	public void setElementPainter(ElementPainter elementPainter) {
		this.elementPainter = elementPainter;
	}

	public ElementType getElementType() {
		return elementType;
	}

	public void setElementType(ElementType elementType) {
		this.elementType = elementType;
	}

}
