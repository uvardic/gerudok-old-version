package model.elements.deviceElements;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Paint;
import java.awt.Stroke;
import java.awt.geom.Point2D;

import model.elements.ElementType;
import model.elements.SlotElement;
import painters.devicePainters.CirclePainter;
import utilities.Constants;

public class CircleElement extends SlotDevice {

	private static final long serialVersionUID = 1961162924576085060L;

	public CircleElement(Paint paint, Stroke stroke, Color strokeColor, Dimension size, Point2D point) {
		super(paint, stroke, strokeColor, size, point, 2, 1);
		elementPainter = new CirclePainter(this);
		elementType = ElementType.CIRCLE;
	}

	public CircleElement(CircleElement circle) {
		super(circle);
		setName(getName() + " Copy");
		elementPainter = new CirclePainter(this);
	}

	public static SlotDevice createDefault(Point2D point, int index) {
		Point2D pos = (Point2D) point.clone();
		BasicStroke stroke = new BasicStroke((2), BasicStroke.CAP_SQUARE, BasicStroke.JOIN_BEVEL);
		Dimension size = new Dimension(50, 50);
		CircleElement element = new CircleElement(Constants.DEVICE_COLOR, stroke, Color.BLACK, size, pos);
		element.setName("Circle - " + index);

		return element;
	}

	@Override
	public SlotElement clone() {
		return new CircleElement(this);
	}

}
