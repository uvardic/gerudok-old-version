package model.elements.deviceElements;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Paint;
import java.awt.Stroke;
import java.awt.geom.Point2D;

import model.elements.ElementType;
import model.elements.SlotElement;
import painters.devicePainters.TextPainter;
import view.gui.Dialog;

public class TextElement extends SlotDevice {

	private static final long serialVersionUID = 1536388763786674481L;

	private String text;

	public TextElement(Paint paint, Stroke stroke, Color strokeColor, Dimension size, Point2D point) {
		super(paint, stroke, strokeColor, size, point, 0, 0);
		text = Dialog.inputDialog();
		elementType = ElementType.TEXT;
		elementPainter = new TextPainter(this);
	}

	public TextElement(TextElement text) {
		super(text);
		this.text = text.getText();
		setName(text.getName() + " Copy");
		elementPainter = new TextPainter(this);
	}

	public static SlotDevice createDefault(Point2D point, int index) {
		BasicStroke stroke = new BasicStroke((2), BasicStroke.CAP_SQUARE, BasicStroke.JOIN_BEVEL);
		Dimension size = new Dimension(0, 0);
		TextElement element = new TextElement(Color.WHITE, stroke, Color.WHITE, size, point);
		element.setName("Text - " + index);
		element.setSize(element.getTextElementSize(element.getText().length()));

		return element;
	}

	@Override
	public SlotElement clone() {
		return new TextElement(this);
	}

	private Dimension getTextElementSize(int lenght) {
		if (lenght < 5) {
			return new Dimension(50, 30);
		} else {
			return new Dimension(lenght * 10, 30);
		}
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
