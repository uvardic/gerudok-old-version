package model.elements.deviceElements;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Paint;
import java.awt.Stroke;
import java.awt.geom.Point2D;

import model.elements.ElementType;
import model.elements.SlotElement;
import painters.devicePainters.RectanglePainter;
import utilities.Constants;

public class RectangleElement extends SlotDevice {

	private static final long serialVersionUID = -4726858578138583924L;

	public RectangleElement(Paint paint, Stroke stroke, Color strokeColor, Dimension size, Point2D point) {
		super(paint, stroke, strokeColor, size, point, 2, 1);
		elementPainter = new RectanglePainter(this);
		elementType = ElementType.RECTANGLE;
	}

	public RectangleElement(RectangleElement rectangle) {
		super(rectangle);
		setName(rectangle.getName() + " Copy");
		elementPainter = new RectanglePainter(this);
	}

	public static SlotDevice createDefault(Point2D point, int index) {
		BasicStroke stroke = new BasicStroke((2), BasicStroke.CAP_SQUARE, BasicStroke.JOIN_BEVEL);
		Dimension size = new Dimension(80, 50);
		RectangleElement element = new RectangleElement(Constants.DEVICE_COLOR, stroke, Color.BLACK, size, point);
		element.setName("Rectangle - " + index);

		return element;
	}

	@Override
	public SlotElement clone() {
		return new RectangleElement(this);
	}

}