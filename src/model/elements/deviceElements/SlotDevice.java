package model.elements.deviceElements;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Paint;
import java.awt.Stroke;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import model.elements.ElementType;
import model.elements.InputOutputElement;
import model.elements.SlotElement;

public abstract class SlotDevice extends SlotElement {

	private static final long serialVersionUID = 5422401342622200451L;

	protected Dimension size;
	protected Point2D point;
	protected double scale;
	protected double rotation;

	// Broj inputa i outputa
	protected int numberOfInputs;
	protected int numberOfOutputs;

	// Lista svih inputa i outputa
	protected ArrayList<InputOutputElement> inputs = new ArrayList<>();
	protected ArrayList<InputOutputElement> outputs = new ArrayList<>();

	public SlotDevice(Paint paint, Stroke stroke, Color strokeColor, Dimension size, Point2D point, int numberOfInputs,
			int numberOfOutputs) {
		super(paint, stroke, strokeColor);
		this.size = size;
		// Postavljanje pozicije tako da mis bude na sredini kada crtamo element
		point.setLocation(point.getX() - size.getWidth() / 2, point.getY() - size.getHeight() / 2);
		this.point = point;
		this.numberOfInputs = numberOfInputs;
		this.numberOfOutputs = numberOfOutputs;
		scale = 1;
		rotation = 0;

		// Kreiranje inputa
		for (int i = 0; i < numberOfInputs; i++) {
			Point2D inputPoint = new Point2D.Double(point.getX(),
					point.getY() + (size.getHeight() / (numberOfInputs + 1) * (i + 1)));
			addInput((InputOutputElement) InputOutputElement.createDefault(inputPoint, this, i + 1, ElementType.INPUT));
		}

		// Kreiranje outputa
		for (int i = 0; i < numberOfOutputs; i++) {
			Point2D outputPoint = new Point2D.Double(point.getX() + size.getWidth(),
					point.getY() + (size.getHeight() / (numberOfOutputs + 1) * (i + 1)));
			addOutput((InputOutputElement) InputOutputElement.createDefault(outputPoint, this, i + 1,
					ElementType.OUTPUT));
		}
	}

	public SlotDevice(SlotDevice device) {
		super(device);
		this.size = device.size;
		this.point = device.point;
		this.numberOfInputs = device.numberOfInputs;
		this.numberOfOutputs = device.numberOfOutputs;
		scale = 1;
		rotation = 0;

		for (int i = 0; i < device.inputs.size(); i++) {
			this.inputs.add(new InputOutputElement(device.inputs.get(i), this));
		}

		for (int i = 0; i < device.outputs.size(); i++) {
			this.outputs.add(new InputOutputElement(device.outputs.get(i), this));
		}
	}

	// ----------------------INPUTS----------------------------------------------------------------

	public void addInput(InputOutputElement input) {
		inputs.add(input);
	}

	public void removeInput(InputOutputElement input) {
		inputs.remove(input);
	}

	public InputOutputElement getInputAt(int i) {
		return inputs.get(i);
	}

	public InputOutputElement getFirstInput() {
		return inputs.get(0);
	}

	// Vraca najblizi input
	public InputOutputElement getClosestInput(Point2D point) {
		InputOutputElement closest = getFirstInput();

		for (InputOutputElement input : inputs) {
			if (point.distance(input.getPoint()) < point.distance(closest.getPoint())) {
				closest = input;
			}
		}

		return closest;
	}

	// ----------------------OUTPUTS---------------------------------------------------------------

	public void addOutput(InputOutputElement output) {
		outputs.add(output);
	}

	public void removeOutput(InputOutputElement output) {
		outputs.remove(output);
	}

	public InputOutputElement getOutputAt(int i) {
		return outputs.get(i);
	}

	public InputOutputElement getFirstOutput() {
		return outputs.get(0);
	}

	// ----------------------GETTERS---------------------------------------------------------------

	public double getPointX() {
		return point.getX();
	}

	public double getPointY() {
		return point.getY();
	}

	public double getWidth() {
		return getSize().getWidth();
	}

	public double getHeight() {
		return getSize().getHeight();
	}

	public Dimension getSize() {
		Dimension scaledSize = new Dimension();
		scaledSize.setSize(size.getWidth() * scale, size.getHeight() * scale);
		return scaledSize;
	}

	public Dimension getInitialSize() {
		return size;
	}

	public void setSize(Dimension size) {
		this.size = size;
	}

	public Point2D getPoint() {
		return point;
	}

	public void setPoint(Point2D point) {
		this.point = point;
	}

	public int getNumberOfInputs() {
		return numberOfInputs;
	}

	public void setNumberOfInputs(int numberOfInputs) {
		this.numberOfInputs = numberOfInputs;
	}

	public int getNumberOfOutputs() {
		return numberOfOutputs;
	}

	public void setNumberOfOutputs(int numberOfOutputs) {
		this.numberOfOutputs = numberOfOutputs;
	}

	public ArrayList<InputOutputElement> getInputs() {
		return inputs;
	}

	public void setInputs(ArrayList<InputOutputElement> inputs) {
		this.inputs = inputs;
	}

	public ArrayList<InputOutputElement> getOutputs() {
		return outputs;
	}

	public void setOutputs(ArrayList<InputOutputElement> outputs) {
		this.outputs = outputs;
	}

	public double getScale() {
		return scale;
	}

	public void setScale(double scale) {
		this.scale = scale;
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}

}
