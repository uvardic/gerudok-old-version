package model.elements.deviceElements;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Paint;
import java.awt.Stroke;
import java.awt.geom.Point2D;

import model.elements.ElementType;
import model.elements.SlotElement;
import painters.devicePainters.TrianglePainter;
import utilities.Constants;

public class TriangleElement extends SlotDevice {

	private static final long serialVersionUID = -5917620551660981911L;

	public TriangleElement(Paint paint, Stroke stroke, Color strokeColor, Dimension size, Point2D point) {
		super(paint, stroke, strokeColor, size, point, 2, 1);
		elementPainter = new TrianglePainter(this);
		elementType = ElementType.TRIANGLE;
	}

	public TriangleElement(TriangleElement triangle) {
		super(triangle);
		setName(triangle.getName() + " Copy");
		elementPainter = new TrianglePainter(this);
	}

	public static SlotDevice createDefault(Point2D point, int index) {
		BasicStroke stroke = new BasicStroke((2), BasicStroke.CAP_SQUARE, BasicStroke.JOIN_BEVEL);
		Dimension size = new Dimension(50, 50);
		TriangleElement element = new TriangleElement(Constants.DEVICE_COLOR, stroke, Color.BLACK, size, point);
		element.setName("Triangle - " + index);

		return element;
	}

	@Override
	public SlotElement clone() {
		return new TriangleElement(this);
	}

}
