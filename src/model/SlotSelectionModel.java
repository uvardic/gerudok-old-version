package model;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.DefaultSingleSelectionModel;
import javax.swing.event.EventListenerList;

import event.UpdateEvent;
import event.UpdateListener;
import model.elements.SlotElement;
import model.elements.deviceElements.SlotDevice;
import model.elements.linkElements.LinkElement;
import painters.linkPainters.LinkPainter;

public class SlotSelectionModel extends DefaultSingleSelectionModel {

	private static final long serialVersionUID = 1L;

	private ArrayList<SlotElement> selectionList = new ArrayList<>();

	private transient EventListenerList listenerList = new EventListenerList();
	private transient UpdateEvent updateEvent = null;

	private Object readResolve() {
		listenerList = new EventListenerList();
		return this;
	}

	public void selectElement(Rectangle2D rectangle, ArrayList<SlotElement> elements) {
		Iterator<SlotElement> it = elements.iterator();

		while (it.hasNext()) {
			SlotElement element = it.next();
			if (element instanceof SlotDevice) {
				SlotDevice device = (SlotDevice) element;
				if (rectangle.intersects(device.getPointX(), device.getPointY(), device.getWidth(),
						device.getHeight())) {
					if (!selectionList.contains(device)) {
						selectionList.add(device);
					}
				}
			} else {
				LinkElement link = (LinkElement) element;
				if (rectangle.contains(LinkPainter.linkSelectionRectangle(link))) {
					if (!selectionList.contains(link)) {
						selectionList.add(link);
					}
				}
			}
		}
	}

	// ----------------------EVENT-----------------------------------------------------------------

	public void addUpdateListener(UpdateListener l) {
		listenerList.add(UpdateListener.class, l);
	}

	public void removeAllFromSelectionList() {
		selectionList.clear();
		fireUpdatePerformed();
	}

	public void removeFromSelectionList(SlotElement element) {
		selectionList.remove(element);
		fireUpdatePerformed();
	}

	public void addToSelectionList(SlotElement element) {
		selectionList.add(element);
		fireUpdatePerformed();
	}

	public void addToSelectionList(ArrayList<SlotElement> list) {
		selectionList.addAll(list);
		fireUpdatePerformed();
	}

	public int getSelectionListSize() {
		return selectionList.size();
	}

	public SlotElement getElementAt(int index) {
		return selectionList.get(index);
	}

	public SlotElement getLastElement() {
		return selectionList.get(selectionList.size() - 1);
	}

	public ArrayList<SlotElement> getSelected() {
		ArrayList<SlotElement> selected = new ArrayList<>();
		selected.addAll(selectionList);

		return selected;
	}

	public void fireUpdatePerformed() {
		Object[] listeners = listenerList.getListenerList();

		for (int i = listeners.length - 1; i >= 0; i -= 1) {
			if (listeners[i] == UpdateListener.class) {
				if (updateEvent == null) {
					updateEvent = new UpdateEvent(this);
				}
				((UpdateListener) listeners[i + 1]).updatePerformed(updateEvent);
			}
		}
	}

	// ----------------------GETTERS---------------------------------------------------------------

	public ArrayList<SlotElement> getSelectionList() {
		return selectionList;
	}

	public void setSelectionList(ArrayList<SlotElement> selectionList) {
		this.selectionList = selectionList;
	}

	public EventListenerList getListenerList() {
		return listenerList;
	}

	public void setListenerList(EventListenerList listenerList) {
		this.listenerList = listenerList;
	}

	public UpdateEvent getUpdateEvent() {
		return updateEvent;
	}

	public void setUpdateEvent(UpdateEvent updateEvent) {
		this.updateEvent = updateEvent;
	}
}
