package model;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.event.EventListenerList;

import event.UpdateEvent;
import event.UpdateListener;
import model.elements.SlotElement;

public class SlotModel implements Serializable {

	private static final long serialVersionUID = 4059877810304406228L;

	private ArrayList<SlotElement> elements = new ArrayList<SlotElement>();
	private transient EventListenerList listenerList = new EventListenerList();
	private transient UpdateEvent updateEvent = null;

	private Object readResolve() {
		listenerList = new EventListenerList();
		return this;
	}

	public int getElementAtPosition(Point2D point) {
		for (int i = 0; i < elements.size(); i++) {
			SlotElement element = elements.get(i);
			if (element.getElementPainter().isElementAt(point)) {
				return i;
			}
		}

		return -1;
	}

	// ----------------------EVENT-----------------------------------------------------------------

	public void addUpdateListener(UpdateListener l) {
		listenerList.add(UpdateListener.class, l);
	}

	public void addElement(SlotElement element) {
		elements.add(element);
		fireUpdatePerformed();
	}

	public void removeElement(SlotElement element) {
		elements.remove(element);
		fireUpdatePerformed();
	}

	public int getElementCount() {
		return elements.size();
	}

	protected void fireUpdatePerformed() {
		Object[] listeners = listenerList.getListenerList();

		for (int i = listeners.length - 1; i >= 0; i--) {
			if (listeners[i] == UpdateListener.class) {
				if (updateEvent == null) {
					updateEvent = new UpdateEvent(this);
				}
				((UpdateListener) listeners[i + 1]).updatePerformed(updateEvent);
			}
		}
	}

	// ----------------------GETTERS---------------------------------------------------------------

	public ArrayList<SlotElement> getElements() {
		return elements;
	}

	public void setElements(ArrayList<SlotElement> elements) {
		this.elements = elements;
	}

	public EventListenerList getListenerList() {
		return listenerList;
	}

	public void setListenerList(EventListenerList listenerList) {
		this.listenerList = listenerList;
	}

	public UpdateEvent getUpdateEvent() {
		return updateEvent;
	}

	public void setUpdateEvent(UpdateEvent updateEvent) {
		this.updateEvent = updateEvent;
	}

}
