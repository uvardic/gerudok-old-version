package utilities;

import java.awt.Color;
import java.awt.Toolkit;

public class Constants {

	public static final int WIDTH = Toolkit.getDefaultToolkit().getScreenSize().width;

	public static final int HEIGHT = Toolkit.getDefaultToolkit().getScreenSize().height;

	public static final int xOFFSET = 30;

	public static final int yOFFSET = 30;

	public static final int HANDLE_SIZE = 7;

	public static final Color DEVICE_COLOR = new Color(36, 105, 170);

	public static final double SCALING_FACTOR = 1.2;

	public static final double TRANSLATE_FACTOR = 10;

}
