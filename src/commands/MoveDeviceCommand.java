package commands;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Iterator;

import model.SlotSelectionModel;
import model.elements.SlotElement;
import model.elements.deviceElements.SlotDevice;

public class MoveDeviceCommand extends AbstractCommand {

	private SlotSelectionModel tmpSelectionModel;
	private boolean commandExecuted;
	private double deltaX;
	private double deltaY;

	private ArrayList<SlotElement> movedElements = new ArrayList<>();

	public MoveDeviceCommand(SlotSelectionModel tmpSelectionModel, double deltaX, double deltaY) {
		for (int i = 0; i < tmpSelectionModel.getSelectionListSize(); i++) {
			SlotElement element = tmpSelectionModel.getElementAt(i);
			if (element instanceof SlotDevice) {
				movedElements.add(element);
			}
		}

		this.tmpSelectionModel = tmpSelectionModel;
		commandExecuted = true;
		this.deltaX = deltaX;
		this.deltaY = deltaY;
	}

	
	
	@Override
	public void doCommand() {
		if (commandExecuted) {
			commandExecuted = false;
		} else {
			tmpSelectionModel.addToSelectionList(movedElements);
			Iterator<SlotElement> it = movedElements.iterator();
			while (it.hasNext()) {
				SlotElement element = it.next();
				
				if (element instanceof SlotDevice) {
					SlotDevice device = (SlotDevice) element;
					Point2D newPoint = device.getPoint();
					newPoint.setLocation(newPoint.getX() + deltaX, newPoint.getY() + deltaY);
					device.setPoint(newPoint);
				}
				
			}
			
			tmpSelectionModel.removeAllFromSelectionList();
		}

	}

	@Override
	public void undoCommand() {
		tmpSelectionModel.addToSelectionList(movedElements);
		Iterator<SlotElement> it = movedElements.iterator();
		while (it.hasNext()) {
			SlotElement element = it.next();
			if (element instanceof SlotDevice) {
				
				SlotDevice device = (SlotDevice) element;
				Point2D newPoint = device.getPoint();
				newPoint.setLocation(newPoint.getX() - deltaX, newPoint.getY() - deltaY);
				device.setPoint(newPoint);
				
			}
			
		}

		tmpSelectionModel.removeAllFromSelectionList();
	}

}
