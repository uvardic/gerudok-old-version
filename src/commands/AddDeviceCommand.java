package commands;

import java.awt.geom.Point2D;

import javax.swing.SwingUtilities;

import core.MainFrame;
import model.SlotModel;
import model.SlotSelectionModel;
import model.elements.ElementType;
import model.elements.deviceElements.CircleElement;
import model.elements.deviceElements.RectangleElement;
import model.elements.deviceElements.SlotDevice;
import model.elements.deviceElements.TextElement;
import model.elements.deviceElements.TriangleElement;
import tree.nodes.ElementNode;
import view.Diagram;
import view.Page;
import view.Slot;

public class AddDeviceCommand extends AbstractCommand {

	private SlotModel slotModel;
	private SlotSelectionModel selectionModel;
	private Point2D point;
	private SlotDevice device = null;
	private ElementType elementType;

	public AddDeviceCommand(SlotModel slotModel, SlotSelectionModel selectionModel, Point2D point,
			ElementType elementType) {
		this.slotModel = slotModel;
		this.selectionModel = selectionModel;
		this.point = point;
		this.elementType = elementType;
	}

	@Override
	public void doCommand() {
		int index = slotModel.getElementCount() + 1;
		if (device == null) {
			if (elementType == ElementType.CIRCLE) {
				device = CircleElement.createDefault(point, index);
			}

			else if (elementType == ElementType.RECTANGLE) {
				device = RectangleElement.createDefault(point, index);
			}

			else if (elementType == ElementType.TRIANGLE) {
				device = TriangleElement.createDefault(point, index);
			}

			else if (elementType == ElementType.TEXT) {
				device = TextElement.createDefault(point, index);
			}
		}

		selectionModel.removeAllFromSelectionList();
		slotModel.addElement(device);
		selectionModel.addToSelectionList(device);

		new ElementNode(device);
		SwingUtilities.updateComponentTreeUI(MainFrame.getInstance().getTree());

	}

	
	
	
	@Override
	public void undoCommand() {
		selectionModel.removeAllFromSelectionList();
		slotModel.removeElement(device);

		Diagram diagram = MainFrame.getInstance().getSelectedDiagram();
		Page page = diagram.getSelectedPage();
		Slot slot = page.getSelectedSlot();
		slot.getSlotNode().getFolders().get(0).removeLastElementNode();
		SwingUtilities.updateComponentTreeUI(MainFrame.getInstance().getTree());
	}

}
