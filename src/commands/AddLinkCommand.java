package commands;

import javax.swing.SwingUtilities;

import core.MainFrame;
import model.SlotModel;
import model.elements.linkElements.LinkElement;
import tree.nodes.ElementNode;
import view.Diagram;
import view.Page;
import view.Slot;

public class AddLinkCommand extends AbstractCommand {

	private SlotModel slotModel;
	private LinkElement link;
	private boolean commandExecuted;

	public AddLinkCommand(SlotModel slotModel, LinkElement link) {
		this.slotModel = slotModel;
		this.link = link;
		commandExecuted = true;
	}

	
	
	@Override
	public void doCommand() {
		if (commandExecuted) {
			commandExecuted = false;
		} else {
			slotModel.addElement(link);
			LinkElement.plusIndex();
			;
			new ElementNode(link);
			SwingUtilities.updateComponentTreeUI(MainFrame.getInstance().getTree());
		}
	}

	
	
	@Override
	public void undoCommand() {
		slotModel.removeElement(link);
		LinkElement.minusIndex();

		Diagram diagram = MainFrame.getInstance().getSelectedDiagram();
		Page page = diagram.getSelectedPage();
		Slot slot = page.getSelectedSlot();
		slot.getSlotNode().getFolders().get(1).removeLastElementNode();
		SwingUtilities.updateComponentTreeUI(MainFrame.getInstance().getTree());
	}

}
