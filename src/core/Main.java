package core;

import com.alee.laf.WebLookAndFeel;

public class Main {

	public static void main(String[] args) throws IllegalAccessException {

		// Throws error on JDK9

		WebLookAndFeel.install();
		MainFrame.getInstance();

		// MainFrame.getInstance();
	}

}
