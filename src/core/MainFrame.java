package core;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import controller.ActionManager;
import tree.TreeModel;
import tree.TreeView;
import utilities.Constants;
import view.Diagram;
import view.gui.Menu;
import view.gui.Palette;
import view.gui.StatusBar;
import view.gui.ToolBar;

public class MainFrame extends JFrame implements ClipboardOwner {

	private static final long serialVersionUID = 1L;

	private static MainFrame instance = null;

	private ActionManager actionManager = null;

	private JSplitPane splitPane = null;
	// Inicijalizacija scroll pane-a za drvo postavljena na null.
	private JScrollPane treeScroll = null;
	private JDesktopPane desktopPane = null;

	// Inicijalizacija MenuBara,toolBar-a i palette.
	private Menu menu = null;
	private ToolBar toolBar = null;
	private Palette palette = null;
	private StatusBar statusBar = null;

	// Inicijalizacija JTree view i modela.
	private TreeView tree = null;
	private TreeModel treeModel = null;

	private Clipboard clipboard = new Clipboard("GeRuDok clipboard");

	// Lista diagrama.
	private ArrayList<Diagram> diagrams = new ArrayList<Diagram>();

	private MainFrame() {
	}

	public static MainFrame getInstance() {
		if (instance == null) {
			instance = new MainFrame();
			instance.initializeFrame();
			instance.initializeComponents();
			instance.setVisible(true);
		}

		return instance;
	}

	// Definisan je naziv projekta, omogućeno potpuno zatvaranje,
	// nakon pokretanja projekat se nalazi preko celog ekrana,
	// a minimizacija je definisana na pola veli�ine.
	private void initializeFrame() {
		setTitle("Gerudok");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setSize(new Dimension(Constants.WIDTH / 2, Constants.HEIGHT / 2));
	}

	// Inicijalizacija komponenata i addovanje na BorderLayout.
	private void initializeComponents() {
		actionManager = new ActionManager();

		menu = new Menu();
		toolBar = new ToolBar();
		palette = new Palette();
		statusBar = new StatusBar();

		treeModel = new TreeModel();
		tree = new TreeView();
		tree.setModel(treeModel);
		treeScroll = new JScrollPane(tree);
		treeScroll.setMinimumSize(new Dimension(50, Constants.HEIGHT));

		desktopPane = new JDesktopPane();

		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, treeScroll, desktopPane);
		splitPane.setDividerLocation(300);

		setJMenuBar(menu);
		add(toolBar, BorderLayout.NORTH);
		add(splitPane, BorderLayout.CENTER);
		add(palette, BorderLayout.EAST);
		add(statusBar, BorderLayout.SOUTH);
	}

	@Override
	public void lostOwnership(Clipboard clipboard, Transferable contents) {
		System.out.println("lostOwnership");
	}

	// ----------------------GETTERS---------------------------------------------------------------

	public Diagram getSelectedDiagram() {
		return (Diagram) desktopPane.getSelectedFrame();
	}

	public int getDiagramCount() {
		return desktopPane.getAllFrames().length;
	}

	public void addDiagram(Diagram diagram) {
		desktopPane.add(diagram);
		diagrams.add(diagram);
	}

	public void removeDiagram(Diagram diagram) {
		desktopPane.remove(diagram);
		diagrams.remove(diagram);
	}

	public void removeAllDiagrams() {
		desktopPane.removeAll();
		diagrams.clear();
	}
	
	public Diagram getLastDiagram() {
		return diagrams.get(diagrams.size() - 1);
	}

	public ArrayList<Diagram> getDiagrams() {
		return diagrams;
	}

	public void setDiagrams(ArrayList<Diagram> diagrams) {
		this.diagrams = diagrams;
	}

	public JSplitPane getSplitPane() {
		return splitPane;
	}

	public void setSplitPane(JSplitPane splitPane) {
		this.splitPane = splitPane;
	}

	public JScrollPane getTreeScroll() {
		return treeScroll;
	}

	public void setTreeScroll(JScrollPane treeScroll) {
		this.treeScroll = treeScroll;
	}

	public JDesktopPane getDesktopPane() {
		return desktopPane;
	}

	public void setDesktopPane(JDesktopPane desktopPane) {
		this.desktopPane = desktopPane;
	}

	public Menu getMenu() {
		return menu;
	}

	public TreeView getTree() {
		return tree;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public ToolBar getToolBar() {
		return toolBar;
	}

	public void setToolBar(ToolBar toolBar) {
		this.toolBar = toolBar;
	}

	public ActionManager getActionManager() {
		return actionManager;
	}

	public void setActionManager(ActionManager actionManager) {
		this.actionManager = actionManager;
	}

	public Palette getPalette() {
		return palette;
	}

	public void setPalette(Palette palette) {
		this.palette = palette;
	}

	public StatusBar getStatusBar() {
		return statusBar;
	}

	public void setStatusBar(StatusBar statusBar) {
		this.statusBar = statusBar;
	}

	public TreeModel getTreeModel() {
		return treeModel;
	}

	public void setTreeModel(TreeModel treeModel) {
		this.treeModel = treeModel;
	}

	public void setTree(TreeView tree) {
		this.tree = tree;
	}

	public Clipboard getClipboard() {
		return clipboard;
	}

	public void setClipboard(Clipboard clipboard) {
		this.clipboard = clipboard;
	}

}
