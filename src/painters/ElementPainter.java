package painters;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Point2D;
import java.io.Serializable;

import model.elements.SlotElement;

public abstract class ElementPainter implements Serializable {

	private static final long serialVersionUID = 4919301905315975926L;

	protected SlotElement element;
	protected Shape shape;

	public ElementPainter(SlotElement element) {
		this.element = element;
	}

	public abstract void paint(Graphics2D g2, SlotElement element);

	public abstract boolean isElementAt(Point2D point);

}
