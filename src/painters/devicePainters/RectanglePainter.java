package painters.devicePainters;

import java.awt.geom.Rectangle2D;

import model.elements.SlotElement;
import model.elements.deviceElements.RectangleElement;

public class RectanglePainter extends DevicePainter {

	private static final long serialVersionUID = 8065588837280750454L;

	public RectanglePainter(SlotElement device) {
		super(device);
		RectangleElement rectangle = (RectangleElement) element;
		shape = new Rectangle2D.Double(0, 0, rectangle.getWidth(), rectangle.getHeight());
	}

}