package painters.devicePainters;

import java.awt.geom.GeneralPath;

import model.elements.SlotElement;
import model.elements.deviceElements.TriangleElement;

public class TrianglePainter extends DevicePainter {

	// Implementacija trougla definisanjem pozicije i svake njegove tacke kao i
	// du�ine i visine.

	private static final long serialVersionUID = 5202833702490782216L;

	public TrianglePainter(SlotElement element) {
		super(element);
		TriangleElement triangle = (TriangleElement) element;

		shape = new GeneralPath();

		((GeneralPath) shape).moveTo(0, 0);

		((GeneralPath) shape).lineTo(0, triangle.getHeight());

		((GeneralPath) shape).lineTo(triangle.getWidth(), triangle.getHeight() / 2);

		((GeneralPath) shape).closePath();

	}

}
