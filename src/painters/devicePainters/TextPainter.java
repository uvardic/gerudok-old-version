package painters.devicePainters;

import java.awt.geom.Rectangle2D;

import model.elements.SlotElement;
import model.elements.deviceElements.TextElement;

public class TextPainter extends DevicePainter {

	private static final long serialVersionUID = 4845490114570160871L;

	public TextPainter(SlotElement element) {
		super(element);
		TextElement text = (TextElement) element;
		shape = new Rectangle2D.Double(0, 0, text.getWidth(), text.getHeight());
	}

}
