package painters.devicePainters;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Iterator;

import model.elements.InputOutputElement;
import model.elements.SlotElement;
import model.elements.deviceElements.SlotDevice;
import model.elements.deviceElements.TextElement;
import painters.ElementPainter;

public class DevicePainter extends ElementPainter {

	private static final long serialVersionUID = 4837721286956268910L;

	public DevicePainter(SlotElement element) {
		super(element);
	}

	@Override
	public void paint(Graphics2D g2, SlotElement element) {
		SlotDevice device = (SlotDevice) element;
		AffineTransform oldTransform = g2.getTransform();

		Iterator<InputOutputElement> inputIterator = device.getInputs().iterator();
		while (inputIterator.hasNext()) {
			InputOutputElement inputElement = inputIterator.next();
			inputElement.getElementPainter().paint(g2, inputElement);
		}

		Iterator<InputOutputElement> outputIterator = device.getOutputs().iterator();
		while (outputIterator.hasNext()) {
			InputOutputElement outputElement = outputIterator.next();
			outputElement.getElementPainter().paint(g2, outputElement);
		}

		g2.translate(device.getPointX(), device.getPointY());
		g2.scale(device.getScale(), device.getScale());

		// Crtanje oblika
		g2.setPaint(element.getStrokeColor());
		g2.setStroke(element.getStroke());
		g2.draw(shape);

		// Bojenje
		g2.setPaint(element.getPaint());
		g2.fill(shape);

		if (device instanceof TextElement) {
			TextElement textElement = (TextElement) device;
			if (textElement.getText() != null) {
				g2.setPaint(Color.BLACK);
				g2.setFont(new Font("Arial", Font.PLAIN, 14));
				g2.drawString(textElement.getText(), 5, (int) textElement.getHeight() / 2);
			}
		}

		g2.setTransform(oldTransform);
	}

	@Override
	public boolean isElementAt(Point2D point) {
		SlotDevice device = (SlotDevice) element;
		Rectangle2D rectangle = new Rectangle2D.Double();
		rectangle.setRect(device.getPointX(), device.getPointY(), device.getWidth(), device.getHeight());

		return rectangle.contains(point);
	}

}
