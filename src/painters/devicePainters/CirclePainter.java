package painters.devicePainters;

import java.awt.geom.Ellipse2D;

import model.elements.SlotElement;
import model.elements.deviceElements.CircleElement;

public class CirclePainter extends DevicePainter {

	// Iscrtavanje elipse pomoću metode jawa.awt.Ellipse2D

	private static final long serialVersionUID = 8419354158463626100L;

	public CirclePainter(SlotElement element) {
		super(element);
		CircleElement circle = (CircleElement) element;
		shape = new Ellipse2D.Double(0, 0, circle.getWidth(), circle.getHeight());
	}
}
