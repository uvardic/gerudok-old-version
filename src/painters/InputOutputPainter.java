package painters;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;

import model.elements.ElementType;
import model.elements.InputOutputElement;
import model.elements.SlotElement;

public class InputOutputPainter extends ElementPainter {

	private static final long serialVersionUID = 3367604079225672425L;

	public InputOutputPainter(InputOutputElement element) {
		super(element);

		InputOutputElement inOutElement = element;
		shape = new GeneralPath();

		// + ili - 7 se koristi za duzinu linije
		if (inOutElement.getType() == ElementType.INPUT) {
			((GeneralPath) shape).moveTo(0, 0);
			((GeneralPath) shape).lineTo(-7, 0);
		} else if (inOutElement.getType() == ElementType.OUTPUT) {
			((GeneralPath) shape).moveTo(0, 0);
			((GeneralPath) shape).lineTo(7, 0);
		}
	}

	@Override
	public void paint(Graphics2D g2, SlotElement element) {
		InputOutputElement inOutElement = (InputOutputElement) element;
		AffineTransform oldTransform = g2.getTransform();

		g2.translate(inOutElement.getPointX(), inOutElement.getPointY());
		// Skaliranje
		g2.setPaint(element.getStrokeColor());
		g2.setStroke(element.getStroke());
		g2.draw(shape);

		g2.setPaint(element.getPaint());
		g2.fill(shape);

		g2.setTransform(oldTransform);
	}

	@Override
	public boolean isElementAt(Point2D point) {
		return false;
	}

}
