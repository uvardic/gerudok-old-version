package painters.linkPainters;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.Iterator;

import model.elements.SlotElement;
import model.elements.linkElements.LinkElement;
import painters.ElementPainter;

public class LinkPainter extends ElementPainter {

	private static final long serialVersionUID = -1108079819998625798L;

	public LinkPainter(SlotElement element) {
		super(element);
	}

	@Override
	public void paint(Graphics2D g2, SlotElement element) {
		g2.setPaint(element.getPaint());
		g2.setStroke(element.getStroke());

		LinkElement link = (LinkElement) element;
		// Prethodna odnosno prva pozicija
		Point2D last = (Point2D) link.getOutput().getPoint().clone();

		// Radi lepseg iscrtavanja
		last.setLocation(last.getX() + 7, last.getY());

		Iterator<Point2D> it = link.getPoints().iterator();
		while (it.hasNext()) {
			Point2D current = it.next();
			g2.drawLine((int) last.getX(), (int) last.getY(), (int) current.getX(), (int) current.getY());
			last = current;
			// Crtanje prekidne tacke, koristi se - 2 da bi mis bio na sredini
			g2.fillRect((int) last.getX() - 2, (int) last.getY() - 2, 5, 5);
		}

		// Ako smo naisli na ulaz crtamo direktnu liniju do njega
		if (link.getInput() != null) {
			g2.drawLine((int) last.getX(), (int) last.getY(), (int) link.getInput().getPointX() - 7,
					(int) link.getInput().getPointY());
		}
	}

	@Override
	public boolean isElementAt(Point2D point) {
		LinkElement link = (LinkElement) element;
		// Ako ne postoji ulaz ne postoji ni element
		if (link.getInput() == null) {
			return false;
		}

		Rectangle rectangle = new Rectangle(0, 0, 0, 0);
		Point2D outputPoint = link.getOutput().getPoint();
		Point2D inputPoint = link.getInput().getPoint();

		// Pravimo prekidnu tacku i proveravamo da li ona sadrzi poziciju
		rectangle.setRect(outputPoint.getX() - 2, outputPoint.getY() - 2, 5, 5);
		if (rectangle.contains(point)) {
			return true;
		}

		rectangle.setRect(inputPoint.getX() - 2, inputPoint.getY() - 2, 5, 5);
		if (rectangle.contains(point)) {
			return true;
		}

		Iterator<Point2D> it = link.getPoints().iterator();
		while (it.hasNext()) {
			Point2D linkPoint = it.next();
			rectangle.setRect(linkPoint.getX() - 2, linkPoint.getY() - 2, 5, 5);

			if (rectangle.contains(point)) {
				return true;
			}
		}

		return false;
	}

	// Pravimo selekcioni pravugaonik linka
	public static Rectangle linkSelectionRectangle(LinkElement link) {
		double minX = 0, minY = 0, maxX = 0, maxY = 0;
		minX = link.getOutput().getPointX();
		minY = link.getOutput().getPointY();
		maxX = link.getOutput().getPointX() + 5;
		maxY = link.getOutput().getPointY() + 5;

		Iterator<Point2D> it = link.getPoints().iterator();
		while (it.hasNext()) {
			Point2D point = it.next();

			minX = point.getX() < minX ? point.getX() : minX;
			maxX = point.getX() > maxX ? point.getX() : maxX;
			minY = point.getY() < minY ? point.getY() : minY;
			maxY = point.getY() > maxY ? point.getY() : maxY;
		}

		Point2D point = link.getInput().getPoint();
		minX = point.getX() < minX ? point.getX() : minX;
		maxX = point.getX() > maxX ? point.getX() : maxX;
		minY = point.getY() < minY ? point.getY() : minY;
		maxY = point.getY() > maxY ? point.getY() : maxY;

		return new Rectangle((int) minX - 5, (int) minY - 5, (int) (maxX - minX + 10), (int) (maxY - minY + 10));
	}

}
